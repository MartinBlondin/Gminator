import PyQt5.QtWidgets as QW
from PyQt5.QtCore import Qt

from definitions import program_data


class LayerList(QW.QListWidget):
    def __init__(self, parent, *args, **kwargs):
        super(LayerList, self).__init__(*args, **kwargs)
        self.parent = parent
        self.setDragEnabled(True)
        self.setDefaultDropAction(Qt.MoveAction)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setContentsMargins(0, 0, 0, 0)

    def startDrag(self, event):
        layer_id = program_data['selected_layer']
        if layer_id != 'root':
            super(LayerList, self).startDrag(event)

    def dropEvent(self, event):
        super(LayerList, self).dropEvent(event)
        layer_id = program_data['selected_layer']
        queue_order = len(program_data['layers']) - 1 - self.row(self.parent.layers[layer_id])
        if queue_order > 0:
            program_data['manager'].change_layer_queue_order(layer_id, queue_order)
        else:
            self.parent.update_layers()
