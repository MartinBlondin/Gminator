import PyQt5.QtWidgets as QW

from PyQt5.QtCore import Qt, pyqtSignal, QRect
from PyQt5.QtSvg import QSvgWidget

from widgets.base_graphics_view_button import BaseGraphicsViewButton
from widgets.base_combo_box_dialog import BaseComboBoxDialog

from definitions import program_data, COLORS, DOWN_ICON_PATH


class ComboBox(BaseGraphicsViewButton):
    currentTextChanged = pyqtSignal(str, object)
    currentIndexChanged = pyqtSignal(int, object)

    def __init__(self, parent=None, hide_selected=False, width=100, pos_type='bottom', *args, **kwargs):
        if parent: args = [parent, *args]
        super(ComboBox, self).__init__(*args, **kwargs, width=width, bradius=2)
        self.dialog = BaseComboBoxDialog(hide_verical_scrollbar=False)
        self.dialog.width = width
        self.dialog.option_selected_changed.connect(self.item_selected)
        self.dialog.closed.connect(self.on_popup_close)
        self.clicked.connect(self.on_click)
        self.hide_selected = hide_selected
        self.selected_item = None
        self.setStyleSheet(program_data['manager'].get_stylesheet())
        self.margin = 6
        self.pos_type = pos_type

        self.label = QW.QLabel('')
        self.label.setStyleSheet('background-color:transparent;{}color:{}'.format(program_data['manager'].get_font(), COLORS['foreground_dark']))
        self.graphics_scene.addWidget(self.label)
        self.label.move(self.margin, (self.height / 2) - (self.label.height() / 2))
        self.label.setFixedWidth(self.width)

        self.arrow = QSvgWidget(DOWN_ICON_PATH)
        self.graphics_scene.addWidget(self.arrow)
        self.arrow.setFixedSize(8, 5)
        self.arrow.setStyleSheet('background-color:transparent;')
        self.arrow.move(self.width - self.arrow.width() - self.margin, (self.height  / 2) - (self.arrow.height() / 2))

        self.data = {}

    def on_click(self):
        if len(self.dialog.options) > 1:
            self.showPopup()

    def setText(self, text):
        if len(text) > self.width / 10: text = text[:int(self.width / 10)] + '..'
        self.label.setText(text)

    def item_selected(self, item):
        if item in self.data: data = self.data[item]
        else:                 data = None
        self.currentTextChanged.emit(item, data)
        self.currentIndexChanged.emit(list(self.dialog.options.keys()).index(item), data)
        self.selected_item = item
        self.setText(item)

    def addItem(self, item, userData=None):
        if not self.dialog.options:
            self.setText(item)
            self.selected_item = item
        if userData:
            self.data[item] = userData
        self.dialog.add_option(item)

    def setCurrentText(self, item):
        self.setText(item)
        self.selected_item = item

    def setCurrentIndex(self, index):
        item = list(self.dialog.options.keys())[index]
        self.setText(item)
        self.selected_item = item

    def on_popup_close(self):
        self.ignore_mouse_events = False
        self.updateMouseInsidePanel()
        self.set_hovered(self.mouseInsidePanel)

    def clear(self):
        self.setText('')

    def showPopup(self):
        if self.hide_selected: ignored_options = [self.selected_item]
        else:                  ignored_options = []
        pos = self.parent().mapToGlobal(self.pos())
        if self.pos_type == 'top':
            pos.setY(pos.y() + self.height)
        self.ignore_mouse_events = True
        self.dialog.start(pos, pos_type=self.pos_type, ignored_options=ignored_options)
