import PyQt5.QtWidgets as QW
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt, pyqtSignal

from collections import OrderedDict

from definitions import program_data


class BaseComboBoxDialog(QW.QDialog):
    option_selected_changed = pyqtSignal(str)
    closed = pyqtSignal()

    def __init__(self, alignment=Qt.AlignLeft, hide_verical_scrollbar=True):
        super(BaseComboBoxDialog, self).__init__()
        self.setStyleSheet(program_data['manager'].get_stylesheet())
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Popup)
        self.alignment = alignment

        self.layout = QW.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.width = 75
        self.height = 33

        self.options = OrderedDict()

        self.options_list = QW.QListWidget()
        self.layout.addWidget(self.options_list)
        self.ignored_options = []

        self.options_list.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        if hide_verical_scrollbar:
            self.options_list.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.connect_signals()

    def connect_signals(self):
        QW.QShortcut(QKeySequence("escape"), self).activated.connect(self.close)
        self.rejected.connect(self.close)

        self.options_list.itemSelectionChanged.connect(self.option_selected)

    def add_option(self, name, on_click=lambda: None):
        self.options[name] = on_click
        self.update_options()

    def update_options(self):
        self.options_list.clear()
        for option in self.options:
            if option in self.ignored_options: continue
            option_item = QW.QListWidgetItem(option)
            option_item.setTextAlignment(self.alignment)
            self.options_list.addItem(option_item)

    def start(self, pos=None, pos_type='top', ignored_options=[]):
        self.ignored_options = ignored_options
        self.update_options()

        if pos:
            height = self.height * (len(self.options) - len(ignored_options))
            geometry = program_data['window'].geometry()
            bottom = geometry.y() + geometry.height()

            self.setFixedSize(self.width, height)

            if pos_type == 'top':
                if pos.y() + height > bottom: y = bottom - height
                else:                         y = pos.y()
            elif pos_type == 'bottom':
                y = pos.y() - height

            self.move(pos.x(), y)

        self.exec_()

    def option_selected(self):
        if self.options_list.selectedItems():
            option = self.options_list.selectedItems()[0].text()
            self.options[option]()
            self.option_selected_changed.emit(option)
            self.close()

    def close(self):
        self.setHidden(True)
        self.accept()
        self.closed.emit()

    def clear(self):
        self.ignored_options = []
        self.options = {}
        self.update_options()
