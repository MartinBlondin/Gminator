from PyQt5.QtCore import QObject, pyqtSignal

from widgets.marker_widget import MarkerWidget
from widgets.timeline.timeline_selection_dialog import TimelineSelectionDialog
from widgets.timeline.timeline_grab_handle import TimelineGrabHandle
from utils.display_on_top import display_on_top

from definitions import program_data


class TimelineSelectionManager(QObject):
    move_frames = pyqtSignal(object)

    def __init__(self, parent):
        super(TimelineSelectionManager, self).__init__()
        self.parent = parent
        self.selection_dialog = TimelineSelectionDialog()

        self.selection = {}
        self.selection_x_offset = 0
        self.fake_markers = {}
        self.connect_signals()

        self.handle_width = 18
        self.handle_height = 25

        self.left_handle = TimelineGrabHandle(0, 0, self.handle_width, self.handle_height)
        self.left_handle.setHidden(True)
        self.left_handle_proxy = self.parent.scene.addWidget(self.left_handle)

        self.right_handle = TimelineGrabHandle(0, 0, self.handle_width, self.handle_height)
        self.right_handle.setHidden(True)
        self.right_handle_proxy = self.parent.scene.addWidget(self.right_handle)

        self.copied_keyframes = []

    def connect_signals(self):
        self.move_frames.connect(program_data['manager'].move_keyframes)

        self.selection_dialog.copy.connect(self.copy_selected)
        self.selection_dialog.cut.connect(self.cut_selected)
        self.selection_dialog.paste.connect(self.paste_selected)
        self.selection_dialog.clear.connect(self.clear_copied)
        self.selection_dialog.delete.connect(self.delete_selected)

    def on_scroll(self):
        self.update_grab_handles()

    def on_scale(self):
        self.update_grab_handles()

    def is_empty(self):
        for frame, animator_id, attribute_name in self.get_selection():
            return False
        return True

    def length(self):
        size = 0
        for frame, animator_id, attribute_name in self.get_selection():
            size += 1
        return size

    def get_selection(self):
        for frame, animators in self.selection.items():
            for animator_id, attributes in animators.items():
                for attribute_name in attributes:
                    yield(frame, animator_id, attribute_name)

    def start_dialog(self, pos):
        if self.is_empty() and self.copied_keyframes:
            self.selection_dialog.start(pos, ignored_options=['Delete', 'Copy', 'Cut'])
        elif not self.is_empty() and not self.copied_keyframes:
            self.selection_dialog.start(pos, ignored_options=['Paste', 'Clear'])
        elif not self.is_empty() and self.copied_keyframes:
            self.selection_dialog.start(pos)

    def update_grab_handles(self):
        if not self.is_empty() and self.length() > 1:
            selection_contains_multiple_frames = False
            highest_frame = None
            lowest_frame  = None
            last_frame    = None
            y = self.parent.marker_size / 2
            markers = []
            for frame, animator_id, attribute_name in self.get_selection():
                if last_frame and frame != last_frame:
                    selection_contains_multiple_frames = True
                last_frame = frame
                if [animator_id, attribute_name] not in markers:
                    if attribute_name == 'Header':
                        y += self.parent.animators[animator_id].header_markers[frame].y()
                    else:
                        y += self.parent.animators[animator_id].attribute_markers[frame][attribute_name].y()
                    markers.append([animator_id, attribute_name])
                frame = int(frame)
                if not highest_frame or frame > highest_frame:
                    highest_frame = frame
                if not lowest_frame or frame < lowest_frame:
                    lowest_frame = frame

            y /= len(markers)

            if selection_contains_multiple_frames and not self.parent.view.moving:
                display_on_top(self.right_handle_proxy, self.parent.scene)
                display_on_top(self.left_handle_proxy, self.parent.scene)
                high_x = self.parent.get_x_from_frame(highest_frame + 1) - self.handle_width
                low_x  = self.parent.get_x_from_frame(lowest_frame)
                if high_x > self.parent.lines_start_position:
                    self.right_handle.move(high_x, y)
                    self.right_handle.setHidden(False)
                else:
                    self.right_handle.setHidden(True)
                if low_x > self.parent.lines_start_position:
                    self.left_handle.move(low_x, y)
                    self.left_handle.setHidden(False)
                else:
                    self.left_handle.setHidden(True)
            else:
                self.right_handle.setHidden(True)
                self.left_handle.setHidden(True)
        else:
            self.right_handle.setHidden(True)
            self.left_handle.setHidden(True)


    def delete_selected(self):
        to_delete = []
        for frame, animator_id, attribute_name in list(self.get_selection()):
            if frame == '1': continue
            if attribute_name == 'Header':
                attribute_name = None
            to_delete.append([frame, animator_id, attribute_name])
        self.clear_selection()
        program_data['manager'].delete_keyframes(to_delete)

    def clear_copied(self):
        self.copied_keyframes = []

    def copy_selected(self):
        self.copied_keyframes = []
        for frame, animator_id, attribute_name in self.get_selection():
            self.copied_keyframes.append([frame, animator_id, attribute_name, False])

    def cut_selected(self):
        self.copied_keyframes = []
        for frame, animator_id, attribute_name in self.get_selection():
            self.copied_keyframes.append([frame, animator_id, attribute_name, True])

    def paste_selected(self):
        paste_frame = self.parent.get_frame_from_pos(self.parent.mouse_position)
        to_move = []
        lowest_frame = None
        for frame, animator_id, attribute_name, delete_original in self.copied_keyframes:
            to_move.append([frame, animator_id, attribute_name, delete_original])
            if not lowest_frame or int(frame) < lowest_frame:
                lowest_frame = int(frame)

        to_select = []
        to_really_move = []
        for frame, animator_id, attribute_name, delete_original in to_move:
            new_frame = str(paste_frame + int(frame) - lowest_frame)
            if self.is_selected(frame, animator_id, attribute_name):
                self.deselect(frame, animator_id, attribute_name)
                to_select.append([new_frame, animator_id, attribute_name])
            to_really_move.append([frame, new_frame, animator_id, attribute_name, delete_original])

        self.move_frames.emit(to_really_move)

        for frame, animator_id, attribute_name in to_select:
            self.select(frame, animator_id, attribute_name)

        self.copied_keyframes = []

    def is_selected(self, frame, animator_id, attribute_name):
        if (frame in self.selection.keys()
            and animator_id in self.selection[frame].keys()
            and attribute_name in self.selection[frame][animator_id]):
            return True
        return False

    def refresh_marker(self, frame, animator_id, attribute_name):
        if attribute_name == 'Header':
            marker = self.parent.animators[animator_id].header_markers[frame]
        else:
            marker = self.parent.animators[animator_id].attribute_markers[frame][attribute_name]

        if self.is_selected(frame, animator_id, attribute_name):
            marker.set_selected(True)
        else:
            marker.set_selected(False)

    def refresh_selected(self):
        for frame, animator_id, attribute_name, marker in self.parent.get_all_markers():
            if self.is_selected(frame, animator_id, attribute_name):
                marker.set_selected(True)
            else:
                marker.set_selected(False)

    def select(self, frame, animator_id, attribute_name, fake=False):
        if not fake:
            if frame not in self.selection: self.selection[frame] = {}
            if animator_id not in self.selection[frame]: self.selection[frame][animator_id] = []
            if attribute_name not in self.selection[frame][animator_id]: self.selection[frame][animator_id].append(attribute_name)
            self.update_grab_handles()

        if attribute_name == 'Header':
            self.parent.animators[animator_id].header_markers[frame].set_selected(True)
        else:
            self.parent.animators[animator_id].attribute_markers[frame][attribute_name].set_selected(True)

    def deselect(self, frame, animator_id, attribute_name, fake=False):
        if not fake:
            self.selection[frame][animator_id].remove(attribute_name)
            if not self.selection[frame][animator_id]: self.selection[frame].pop(animator_id)
            if not self.selection[frame]: self.selection.pop(frame)
            self.update_grab_handles()

        if attribute_name == 'Header':
            markers = self.parent.animators[animator_id].header_markers
            if frame in markers:
                markers[frame].set_selected(False)
        else:
            markers = self.parent.animators[animator_id].attribute_markers[frame]
            if attribute_name in markers:
                markers[attribute_name].set_selected(False)

    def clear_selection(self, target_frame=None, target_animator_id=None, target_attribute_name=None):
        to_deselect = []
        for frame, animators in self.selection.items():
            if target_frame and frame != target_frame: continue
            for animator_id, attributes in animators.items():
                if target_animator_id and animator_id != target_animator_id: continue
                for attribute_name in attributes:
                    if target_attribute_name and attribute_name != target_attribute_name: continue
                    to_deselect.append([frame, animator_id, attribute_name])
        for args in to_deselect:
            self.deselect(*args)

    def move_selection(self, start_frame, end_frame):
        self.update_grab_handles()
        if start_frame == end_frame:
            return
        difference = end_frame - start_frame
        to_move = []
        for frame, animator_id, attribute_name in self.get_selection():
            if self.parent.view.pressing_shift:
                delete = False
            else:
                delete = True
            if attribute_name == 'Header':
                to_move.append([frame, str(int(frame) + difference), animator_id, None, delete])
            else:
                to_move.append([frame, str(int(frame) + difference), animator_id, attribute_name, delete])

        self.clear_selection()

        self.move_frames.emit(to_move)
        for frame, new_frame, animator_id, attribute_name, delete in to_move:
            if attribute_name == None: attribute_name = 'Header'
            self.select(new_frame, animator_id, attribute_name)

    def scale_selection(self, start_frame, end_frame):
        difference = end_frame - start_frame
        marker_x_offset = self.parent.frame_distance / 2
        frames = [int(frame) for frame in self.selection]
        low_frame = min(frames)
        high_frame = max(frames)
        selection = list(self.get_selection())
        to_move = []

        for frame, animator_id, attribute_name in selection:
            frame_pos = int(frame) - low_frame
            if frame_pos > 0:
                scale_factor = frame_pos / (high_frame - low_frame)
            else:
                scale_factor = 0
            if self.parent.view.scale_start_handle == 'left':
                scale_factor = (scale_factor - 1) * -1
            target_frame = str(round(int(frame) + (difference * scale_factor)))

            if frame != target_frame:
                self.deselect(frame, animator_id, attribute_name)
                if attribute_name == 'Header':
                    to_move.append([frame, target_frame, animator_id, None, True])
                else:
                    to_move.append([frame, target_frame, animator_id, attribute_name, True])

        self.move_frames.emit(to_move)

        for frame, target_frame, animator_id, attribute_name, delete in to_move:
            if attribute_name == None: attribute_name = 'Header'
            self.select(target_frame, animator_id, attribute_name)

    def clear_selection_markers(self):
        for frame, animator_id, attribute_name in self.get_selection():
            if attribute_name == 'Header':
                marker = self.parent.animators[animator_id].header_markers[frame]
            else:
                marker = self.parent.animators[animator_id].attribute_markers[frame][attribute_name]
            if not self.parent.animators[animator_id].hidden:
                marker.setHidden(False)
        self.parent.hide_selected = False

        for frame, animators in self.fake_markers.items():
            for animator_id, attributes in animators.items():
                for attribute_name, marker in attributes.items():
                    marker.setHidden(True)
                    marker.deleteLater()
        self.fake_markers = {}

    def copy_selection_markers(self):
        for frame, animator_id, attribute_name in self.get_selection():
            if frame not in self.fake_markers.keys(): self.fake_markers[frame] = {}
            if animator_id not in self.fake_markers[frame].keys(): self.fake_markers[frame][animator_id] = {}
            if attribute_name not in self.fake_markers[frame][animator_id].keys():
                self.fake_markers[frame][animator_id][attribute_name] = MarkerWidget(0, 0, self.parent.marker_size / 3, self.parent.marker_size)
                self.fake_markers[frame][animator_id][attribute_name].set_selected(True)
                self.parent.scene.addWidget(self.fake_markers[frame][animator_id][attribute_name])
            self.parent.on_shift_pressed()

    def scale_selection_markers(self, start_frame, end_frame):
        difference = end_frame - start_frame
        marker_x_offset = self.parent.frame_distance / 2
        frames = [int(frame) for frame in self.selection]
        low_frame = min(frames)
        high_frame = max(frames)
        for frame, animators in self.fake_markers.items():
            frame = int(frame)
            frame_pos = frame - low_frame
            if frame_pos > 0:
                scale_factor = frame_pos / (high_frame - low_frame)
            else:
                scale_factor = 0
            if self.parent.view.scale_start_handle == 'left':
                scale_factor = (scale_factor - 1) * -1
            target_frame = round(frame + (difference * scale_factor))
            for animator_id, attributes in animators.items():
                for attribute_name, marker in attributes.items():
                    if attribute_name == 'Header':
                        real_marker = self.parent.animators[animator_id].header_markers[str(frame)]
                    else:
                        real_marker = self.parent.animators[animator_id].attribute_markers[str(frame)][attribute_name]
                    marker.move(self.parent.get_x_from_frame(target_frame) + marker_x_offset - (marker.width / 2), real_marker.y())

        if self.parent.view.scale_start_handle == 'left':
            self.left_handle.move(self.parent.get_x_from_frame(end_frame), self.left_handle.y)
        else:
            self.right_handle.move(self.parent.get_x_from_frame(end_frame + 1) - self.handle_width, self.right_handle.y)

    def move_selection_markers(self):
        if self.parent.hovered_frame:
            self.selection_x_offset = self.parent.get_x_from_frame(self.parent.hovered_frame) + self.parent.frame_distance / 2 - (self.parent.marker_size / 6)
            for frame, animators in self.fake_markers.items():
                for animator_id, attributes in animators.items():
                    for attribute_name, marker in attributes.items():
                        if attribute_name == 'Header':
                            real_marker = self.parent.animators[animator_id].header_markers[frame]
                        else:
                            real_marker = self.parent.animators[animator_id].attribute_markers[frame][attribute_name]
                        marker.move(((int(frame) - self.parent.view.move_start_frame) * self.parent.frame_distance) + self.selection_x_offset, real_marker.y())
            self.update_grab_handles()
