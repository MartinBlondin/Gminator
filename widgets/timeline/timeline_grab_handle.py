from PyQt5.QtCore import QPoint, QRectF
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtGui import QColor, QPainterPath, QPolygon

from utils.get_path_as_string import get_path_as_string

from definitions import GRAPHICS_DIR

HANDLE_BASE_SVG  = get_path_as_string(GRAPHICS_DIR, 'timelineResizeHandle.svg')
HANDLE_HOVER_SVG = get_path_as_string(GRAPHICS_DIR, 'timelineResizeHandleHover.svg')

class TimelineGrabHandle(QSvgWidget):
    def __init__(self, x, y, width, height, *args, **kwargs):
        super(TimelineGrabHandle, self).__init__(HANDLE_BASE_SVG, *args, **kwargs)
        self.move(x, y)
        self.x, self.y = x, y
        self.width = width
        self.height = height
        self.setFixedSize(width, height)
        self.setStyleSheet('background-color: transparent;')
        self.selected = False

    def hover(self, hovered=True):
        self.hovered = hovered
        if self.hovered: self.load(HANDLE_HOVER_SVG)
        else:            self.load(HANDLE_BASE_SVG)

    def move(self, x, y, *args, **kwargs):
        super(TimelineGrabHandle, self).move(x, y, *args, **kwargs)
        self.x = x
        self.y = y

    def rect(self):
        return QRectF(self.x, self.y, self.width, self.height)
