import PyQt5.QtWidgets as QW

from utils.delay import delay
from widgets.rect_widget import RectWidget
from utils.display_on_top import display_on_top

from definitions import program_data, COLORS


class TimelineLinesManager:
    def __init__(self, parent, x, *args, **kwargs):
        super(TimelineLinesManager, self).__init__(*args, **kwargs)
        self.parent = parent
        self.x_offset = x
        self.lines = {}

        self.line_hover = RectWidget(COLORS['timeline_line_selected'], self.x_offset, self.parent.visible_area.y(),
                                     2, self.parent.visible_area.height(), 0.1)
        self.parent.scene.addItem(self.line_hover)
        self.line_hover.setHidden(True)
        self.line_hover_frame = None

        self.line_selected = RectWidget(COLORS['timeline_line_selected'], self.x_offset, self.parent.visible_area.y(),
                                     2, self.parent.visible_area.height(), 1)
        self.parent.scene.addItem(self.line_selected)
        self.line_selected.setHidden(True)

        self.line_playhead = RectWidget(COLORS['timeline_line_playhead'], self.x_offset, self.parent.visible_area.y(),
                                     2, self.parent.visible_area.height(), 1)
        self.parent.scene.addItem(self.line_playhead)
        self.line_playhead.setHidden(True)
        self.playhead_anim_steps = 6
        self.playhead_anim_step = 1

        self.draw_lines()

        self.connect_signals()

    def connect_signals(self):
        program_data['signals'].fps_changed.connect(self.highlight)
        program_data['signals'].keyframe_selected.connect(self.update_selected)
        program_data['signals'].playback_playing_changed.connect(self.update_playhead)
        program_data['signals'].playback_frame_changed.connect(self.move_playhead)

    def update_selected(self, *args, **kwargs):
        self.line_selected.move(self.parent.get_x_from_frame(int(program_data['selected_frame'])),
                                self.parent.visible_area.y())

        if self.line_selected.rect().x() > self.x_offset - 10:
            self.line_selected.setHidden(False)
        else:
            self.line_selected.setHidden(True)

    def hover(self, frame):
        if frame:
            self.line_hover_frame = frame - self.parent.frame_offset + 1
            self.line_hover.move(self.x_offset + ((self.line_hover_frame - 1)
                                                    * self.parent.frame_distance),
                                    self.parent.visible_area.y())
            self.line_hover.setHidden(False)
        else:
            self.line_hover_frame = None
            self.line_hover.setHidden(True)

    def on_scroll(self):
        self.highlight()
        self.update_selected()

    def on_scale(self):
        self.draw_lines()
        self.update_selected()

    def draw_lines(self):
        frame = -1
        x = self.x_offset
        while x < self.parent.visible_area.width():
            frame += 1
            if frame not in self.lines.keys():
                self.lines[frame] = RectWidget(COLORS['timeline_line_dark'], x, self.parent.visible_area.y(),
                                            4, self.parent.visible_area.height())
                self.parent.scene.addItem(self.lines[frame])
            else:
                self.lines[frame].move(x, self.parent.visible_area.y())
                self.lines[frame].setHidden(False)
            x += self.parent.frame_distance

        for line_frame, line in self.lines.items():
            if frame < line_frame:
                line.setHidden(True)

        display_on_top(self.line_hover, self.parent.scene)
        display_on_top(self.line_selected, self.parent.scene)
        display_on_top(self.line_playhead, self.parent.scene)

        self.highlight()
        self.hover(self.line_hover_frame)

    def highlight(self):
        if program_data['fps'] > 8:
            weak_before_strong = program_data['fps'] / 2
        elif program_data['fps'] > 0:
            weak_before_strong = program_data['fps']
        else:
            weak_before_strong = 1
        for frame, line in self.lines.items():
            frame += self.parent.frame_offset
            if frame % weak_before_strong:
                line.setColor(COLORS['timeline_line_dark'])
            else:
                line.setColor(COLORS['timeline_line_light'])

    def progress_playhead(self):
        if self.playhead_anim_step > 0:
            self.move_playhead(int(program_data['playback_frame']) + ((1 / self.playhead_anim_steps) * self.playhead_anim_step))
        self.playhead_anim_step += 1
        if self.playhead_anim_step <= self.playhead_anim_steps:
            self.line_playhead_timer = delay(self.progress_playhead, ((1 / program_data['fps']) * 1000) / self.playhead_anim_steps)

    def update_playhead(self):
        if program_data['playback_playing']:
            self.line_playhead.setHidden(False)
        else:
            self.line_playhead.setHidden(True)
            self.line_playhead_timer.stop()

    def move_playhead(self, frame):
        if float(frame).is_integer():
            frame = int(frame)
            self.playhead_anim_step = 0
            self.progress_playhead()

        frame -= self.parent.frame_offset
        x = self.x_offset + (self.parent.frame_distance * (frame))
        if x > self.parent.lines_start_position:
            self.line_playhead.setHidden(False)
            self.line_playhead.move(x, 0)
        else:
            self.line_playhead.setHidden(True)
