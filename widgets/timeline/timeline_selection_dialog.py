from PyQt5.QtCore import Qt, pyqtSignal

from widgets.base_combo_box_dialog import BaseComboBoxDialog

from definitions import program_data


class TimelineSelectionDialog(BaseComboBoxDialog):
    copy   = pyqtSignal()
    cut    = pyqtSignal()
    paste  = pyqtSignal()
    clear  = pyqtSignal()
    delete = pyqtSignal()

    def __init__(self):
        super(TimelineSelectionDialog, self).__init__(Qt.AlignCenter)
        self.setWindowTitle('Selection')
        self.options = {
            'Copy':   self.copy.emit,
            'Cut':    self.cut.emit,
            'Paste':  self.paste.emit,
            'Clear':  self.clear.emit,
            'Delete': self.delete.emit,
        }
        self.update_options()
