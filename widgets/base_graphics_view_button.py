import PyQt5.QtWidgets as QW

from PyQt5.QtCore import Qt, pyqtSignal, QRect, QRectF
from PyQt5.QtGui import QIcon, QCursor

from widgets.rect_widget import RectWidget

from definitions import program_data, COLORS


class BaseGraphicsViewButton(QW.QGraphicsView):
    clicked         = pyqtSignal()
    right_clicked   = pyqtSignal()
    hovered_changed = pyqtSignal(bool)
    hidden_changed  = pyqtSignal(bool)
    enabled_changed = pyqtSignal(bool)

    def __init__(self, parent=None, width=100, height=33, bradius=4, *args, **kwargs):
        if parent: args = [parent, *args]
        super(BaseGraphicsViewButton, self).__init__(*args, **kwargs)
        self.graphics_scene = QW.QGraphicsScene()
        self.setScene(self.graphics_scene)
        self.pressing_mouse = False
        self.mouseInsidePanel = False
        self.setMouseTracking(True)
        self.enabled = True
        self.hovered = False
        self.width = width
        self.height = height
        self.setStyleSheet('background-color:transparent')
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.bradius = bradius
        self.ignore_mouse_events = False

        self.setMaximumSize(self.width, self.height)
        self.setMinimumSize(self.width, self.height)

        self.outline = RectWidget(COLORS['button_outline'], 0, 0, self.width, self.height, bradius=self.bradius)
        self.graphics_scene.addItem(self.outline)

        margin = 2
        self.background = RectWidget(COLORS['button_background'], margin / 2, margin / 2, self.width - margin, self.height - margin, bradius=self.bradius)
        self.graphics_scene.addItem(self.background)

    def updateMouseInsidePanel(self):
        pos = self.mapFromGlobal(QCursor.pos())
        margin = 4
        rect = self.background.rect()
        rect = QRectF(rect.x() + margin, rect.y() + margin,
                      rect.width()  - (margin * 2),
                      rect.height() - (margin * 2))
        if rect.contains(pos): self.mouseInsidePanel = True
        else:                  self.mouseInsidePanel = False

    def set_hovered(self, hovered):
        self.hovered = hovered
        if self.hovered: self.background.setColor(COLORS['button_hover'])
        else:            self.background.setColor(COLORS['button_background'])
        self.hovered_changed.emit(self.hovered)

    def mouseMoveEvent(self, event):
        super(BaseGraphicsViewButton, self).mouseMoveEvent(event)
        if self.enabled and not self.ignore_mouse_events:
            self.updateMouseInsidePanel()
            if self.pressing_mouse: self.set_hovered(not self.mouseInsidePanel)
            else:                   self.set_hovered(self.mouseInsidePanel)

    def mousePressEvent(self, event):
        super(BaseGraphicsViewButton, self).mousePressEvent(event)
        if self.enabled and not self.ignore_mouse_events:
            if event.button() == 1:
                self.pressing_mouse = True
                self.set_hovered(False)

    def mouseReleaseEvent(self, event, passthrough=False):
        super(BaseGraphicsViewButton, self).mouseReleaseEvent(event)
        if self.enabled and not self.ignore_mouse_events and not passthrough:
            if event.button() == 1:
                self.pressing_mouse = False
                if self.mouseInsidePanel: self.clicked.emit()
            elif event.button() == 2:
                if self.mouseInsidePanel: self.right_clicked.emit()

    def hide(self, hidden):
        self.setEnabled(not hidden)
        self.setHidden(hidden)
        self.hidden_changed.emit(hidden)

    def setEnabled(self, enabled):
        self.enabled = enabled
        self.enabled_changed.emit(enabled)
        if enabled: self.background.setColor(COLORS['button_background'])
        else:       self.background.setColor(COLORS['button_disabled'])

    def focusOutEvent(self, event):
        super(BaseGraphicsViewButton, self).focusOutEvent(event)
        self.set_hovered(False)
