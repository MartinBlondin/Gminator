import PyQt5.QtWidgets as QW
from PyQt5.QtCore import Qt, pyqtSignal

from widgets.qmlComponents.AnimatorKeyframeSettingsDialogLayout import Ui_AnimatorKeyframeSettingsDialogLayout
from widgets.icon_button import IconButton
from widgets.combo_box import ComboBox
from widgets.base_combo_box_dialog import BaseComboBoxDialog
from widgets.editors.slider_attribute_editor import SliderAttributeEditor

from utils.get_path_as_string import get_path_as_string

from definitions import program_data, GRAPHICS_DIR, PLUS_ICON_PATH, MINUS_ICON_PATH


class AnimatorKeyframeSettingsDialog(QW.QDialog):
    def __init__(self):
        super(AnimatorKeyframeSettingsDialog, self).__init__()
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.Popup)

        self.x, self.y = 0, 0
        self.animator_id = None
        self.attribute_name = None

        self.init_ui()
        self.connect_signals()

    def init_ui(self):
        self.setStyleSheet(program_data['manager'].get_stylesheet())

        self.layout = QW.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.generators_by_name = {}
        self.generators = {}
        self.sliders = {}

        self.ui = QW.QWidget()
        Ui_AnimatorKeyframeSettingsDialogLayout().setupUi(self.ui)
        self.layout.addWidget(self.ui)

        self.children = {
            'interpolationContainer':    self.ui.findChild(QW.QHBoxLayout, 'interpolationContainer'),
            'connectionButtonContainer': self.ui.findChild(QW.QHBoxLayout, 'connectionButtonContainer'),
            'connectionList':            self.ui.findChild(QW.QListView,   'connectionList'),

            'interpolationComboBox': ComboBox(),

            'connectionAddButton':    IconButton(PLUS_ICON_PATH, 30, 30),
            'connectionRemoveButton': IconButton(MINUS_ICON_PATH, 30, 30),
            'connectionDialog':       BaseComboBoxDialog(),
        }

        self.children['interpolationContainer'].addWidget(QW.QLabel('Interpolation'))
        self.children['interpolationContainer'].addStretch()
        self.children['interpolationContainer'].addWidget(self.children['interpolationComboBox'])
        self.children['interpolationComboBox'].addItem('Linear')

        self.children['connectionButtonContainer'].addWidget(QW.QLabel('Connections'))
        self.children['connectionButtonContainer'].addStretch()
        self.children['connectionButtonContainer'].addWidget(self.children['connectionAddButton'])
        self.children['connectionButtonContainer'].addWidget(self.children['connectionRemoveButton'])

        self.children['connectionRemoveButton'].setEnabled(False)

    def connect_signals(self):
        self.children['connectionAddButton'].clicked.connect(self.add_connection_button_clicked)
        self.children['connectionDialog'].option_selected_changed.connect(self.connection_selected)

    def add_connection_item(self, generator_id):
        if generator_id not in self.generators:
            self.generators[generator_id] = QW.QListWidgetItem(program_data['generators'][generator_id]['name'])
            self.generators[generator_id].setFlags(self.generators[generator_id].flags() & ~Qt.ItemIsSelectable)
        if generator_id not in self.sliders:
            self.sliders[generator_id] = SliderAttributeEditor()
            self.sliders[generator_id].setFixedWidth(80)
            self.sliders[generator_id].setMaximum(100)
            self.sliders[generator_id].setValue(
                program_data['manager'].get_attribute_value_at_frame(
                    program_data['selected_frame'], generator_id,
                    program_data['generators'][generator_id]['connections'][self.animator_id][self.attribute_name]['Amplitude'])*100)
        container = QW.QWidget()
        container.setStyleSheet('background:transparent')
        container.setLayout(QW.QHBoxLayout())
        container.layout().addStretch()
        container.layout().addWidget(self.sliders[generator_id])
        self.children['connectionList'].addItem(self.generators[generator_id])
        self.children['connectionList'].setItemWidget(self.generators[generator_id], container)

    def add_connection_button_clicked(self):
        for generator_id, generator in program_data['generators'].items():
            if not generator['name'] in self.children['connectionDialog'].options:
                self.children['connectionDialog'].add_option(generator['name'])
                self.generators_by_name[generator['name']] = generator_id
        ignored_options = []
        for generator_id in program_data['manager'].get_generators_by_connection(self.animator_id, self.attribute_name):
            ignored_options.append(program_data['generators'][generator_id]['name'])
        self.children['connectionDialog'].start(ignored_options=ignored_options)

    def connection_selected(self, generator_name):
        generator_id = self.generators_by_name[generator_name]
        program_data['manager'].add_generator_connection(generator_id, self.animator_id, self.attribute_name)
        self.add_connection_item(generator_id)

    def setup(self, pos, animator_id, attribute_name):
        self.setWindowTitle(attribute_name)
        self.animator_id = animator_id
        self.attribute_name = attribute_name
        self.x, self.y = pos.x() + 1, pos.y() + 1
        self.move(self.x, self.y)
        for generator_id in program_data['generators']:
            if generator_id in self.generators:
                self.children['connectionList'].takeItem(self.children['connectionList'].row(self.generators[generator_id]))
                self.generators.pop(generator_id)
                if generator_id in self.sliders:
                    self.sliders.pop(generator_id).deleteLater()
        for generator_id in program_data['manager'].get_generators_by_connection(self.animator_id, self.attribute_name):
            self.add_connection_item(generator_id)
        self.exec_()

    def done(self, *args, **kwargs):
        self.animator_id = None
        super(AnimatorKeyframeSettingsDialog, self).done(*args, **kwargs)
