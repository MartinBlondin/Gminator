# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/martin/python/qt/Gator/static/qml/AnimatorKeyframeSettingsDialogLayout.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AnimatorKeyframeSettingsDialogLayout(object):
    def setupUi(self, AnimatorKeyframeSettingsDialogLayout):
        AnimatorKeyframeSettingsDialogLayout.setObjectName("AnimatorKeyframeSettingsDialogLayout")
        AnimatorKeyframeSettingsDialogLayout.resize(253, 201)
        self.horizontalLayout = QtWidgets.QHBoxLayout(AnimatorKeyframeSettingsDialogLayout)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.interpolationContainer = QtWidgets.QHBoxLayout()
        self.interpolationContainer.setObjectName("interpolationContainer")
        self.verticalLayout.addLayout(self.interpolationContainer)
        self.connectionButtonContainer = QtWidgets.QHBoxLayout()
        self.connectionButtonContainer.setObjectName("connectionButtonContainer")
        self.verticalLayout.addLayout(self.connectionButtonContainer)
        self.connectionList = QtWidgets.QListWidget(AnimatorKeyframeSettingsDialogLayout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.connectionList.sizePolicy().hasHeightForWidth())
        self.connectionList.setSizePolicy(sizePolicy)
        self.connectionList.setMinimumSize(QtCore.QSize(0, 100))
        self.connectionList.setMaximumSize(QtCore.QSize(16777215, 100))
        self.connectionList.setObjectName("connectionList")
        self.verticalLayout.addWidget(self.connectionList)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(AnimatorKeyframeSettingsDialogLayout)
        QtCore.QMetaObject.connectSlotsByName(AnimatorKeyframeSettingsDialogLayout)

    def retranslateUi(self, AnimatorKeyframeSettingsDialogLayout):
        _translate = QtCore.QCoreApplication.translate
        AnimatorKeyframeSettingsDialogLayout.setWindowTitle(_translate("AnimatorKeyframeSettingsDialogLayout", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AnimatorKeyframeSettingsDialogLayout = QtWidgets.QWidget()
    ui = Ui_AnimatorKeyframeSettingsDialogLayout()
    ui.setupUi(AnimatorKeyframeSettingsDialogLayout)
    AnimatorKeyframeSettingsDialogLayout.show()
    sys.exit(app.exec_())
