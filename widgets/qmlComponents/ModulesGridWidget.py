# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/martin/python/qt/Gator/static/qml/ModulesGridWidget.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ModulesGridWidget(object):
    def setupUi(self, ModulesGridWidget):
        ModulesGridWidget.setObjectName("ModulesGridWidget")
        ModulesGridWidget.resize(415, 345)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(ModulesGridWidget.sizePolicy().hasHeightForWidth())
        ModulesGridWidget.setSizePolicy(sizePolicy)
        ModulesGridWidget.setMinimumSize(QtCore.QSize(0, 315))
        self.horizontalLayout = QtWidgets.QHBoxLayout(ModulesGridWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.modulesScrollArea = QtWidgets.QScrollArea(ModulesGridWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.modulesScrollArea.sizePolicy().hasHeightForWidth())
        self.modulesScrollArea.setSizePolicy(sizePolicy)
        self.modulesScrollArea.setMinimumSize(QtCore.QSize(400, 0))
        self.modulesScrollArea.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.modulesScrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.modulesScrollArea.setFrameShadow(QtWidgets.QFrame.Raised)
        self.modulesScrollArea.setLineWidth(0)
        self.modulesScrollArea.setMidLineWidth(1)
        self.modulesScrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.modulesScrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.modulesScrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.modulesScrollArea.setWidgetResizable(True)
        self.modulesScrollArea.setObjectName("modulesScrollArea")
        self.modulesArea = QtWidgets.QWidget()
        self.modulesArea.setGeometry(QtCore.QRect(0, 0, 415, 345))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.modulesArea.sizePolicy().hasHeightForWidth())
        self.modulesArea.setSizePolicy(sizePolicy)
        self.modulesArea.setMinimumSize(QtCore.QSize(0, 0))
        self.modulesArea.setObjectName("modulesArea")
        self.addButtonContainer = QtWidgets.QFrame(self.modulesArea)
        self.addButtonContainer.setGeometry(QtCore.QRect(0, 0, 50, 311))
        self.addButtonContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.addButtonContainer.setFrameShadow(QtWidgets.QFrame.Plain)
        self.addButtonContainer.setObjectName("addButtonContainer")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.addButtonContainer)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.modulesScrollArea.setWidget(self.modulesArea)
        self.horizontalLayout.addWidget(self.modulesScrollArea)

        self.retranslateUi(ModulesGridWidget)
        QtCore.QMetaObject.connectSlotsByName(ModulesGridWidget)

    def retranslateUi(self, ModulesGridWidget):
        _translate = QtCore.QCoreApplication.translate
        ModulesGridWidget.setWindowTitle(_translate("ModulesGridWidget", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ModulesGridWidget = QtWidgets.QWidget()
    ui = Ui_ModulesGridWidget()
    ui.setupUi(ModulesGridWidget)
    ModulesGridWidget.show()
    sys.exit(app.exec_())
