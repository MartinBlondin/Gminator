from PyQt5.QtGui import QPolygonF, QPainterPath, QColor, QPainter
from PyQt5.QtCore import QPointF, Qt
import PyQt5.QtWidgets as QW

from widgets.rect_widget import RectWidget

from definitions import program_data, GENERATOR_SIGNALS, COLORS


class Wave(RectWidget):
    def __init__(self, x, y, width, height):
        self.wave = [0.0]
        super(Wave, self).__init__(COLORS['foreground_dark'], x, y, width, height, opacity=0.5)
        self.paint_lambda = lambda self, painter: painter.fillPath(self.path, self.qcolor)

    def set_wave(self, wave):
        self.wave = wave
        self.path = self.get_path()

    def get_path(self):
        polygon = QPolygonF()
        samples = len(self.wave)
        x = self.rect().x()
        points = []
        fill = 2.3
        for y in self.wave:
            y = (y * self.rect().height()) + self.rect().y()
            point = QPointF(x, y)
            points.append(point)
            polygon.append(point)
            x += self.rect().width() / samples
        for point in reversed(points):
            point.setY(point.y() + fill*2)
            polygon.append(point)
        path = QPainterPath()
        path.addPolygon(polygon)
        return path

    def paint(self, painter, *args, **kwargs):
        painter.setRenderHint(QPainter.HighQualityAntialiasing)
        self.paint_lambda(self, painter)

class WaveDisplay(QW.QWidget):
    def __init__(self, generator_id):
        super(WaveDisplay, self).__init__()
        self.setLayout(QW.QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.generator_id = generator_id

        self.scene = QW.QGraphicsScene()
        self.view = QW.QGraphicsView()
        self.view.setScene(self.scene)
        self.layout().addWidget(self.view)
        self.view.setContentsMargins(0, 0, 0, 0)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.background = RectWidget(COLORS['background_dark'], 0, 0, self.width(), self.height())
        self.scene.addItem(self.background)

        width = 227
        self.setFixedWidth(width)

        self.wave = Wave(3, 25, self.width(), 20)
        self.scene.addItem(self.wave)
        self.update_wave()

    def update_wave(self, frame=None):
        if not frame: frame = program_data['selected_frame']
        self.wave.set_wave(program_data['manager'].get_generator_wave(frame, self.generator_id, 12, 2))
        self.view.update()
