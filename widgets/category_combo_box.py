import PyQt5.QtWidgets as QW

from PyQt5.QtCore import Qt, pyqtSignal, QRect
from PyQt5.QtSvg import QSvgWidget

from widgets.combo_box import ComboBox
from widgets.category_combo_box_dialog import CategoryComboBoxDialog

from definitions import program_data


class CategoryComboBox(ComboBox):
    def __init__(self, parent=None, *args, **kwargs):
        if parent: args = [parent, *args]
        super(CategoryComboBox, self).__init__(*args, **kwargs)
        self.dialog.width = 300
