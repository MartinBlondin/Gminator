import PyQt5.QtWidgets as QW
from collections import OrderedDict
from PyQt5.QtCore import Qt, pyqtSignal

from widgets.base_combo_box_dialog import BaseComboBoxDialog

from definitions import program_data


class CategoryComboBoxDialog(QW.QDialog):
    option_selected_changed = pyqtSignal(str)
    closed = pyqtSignal()

    def __init__(self, alignment=Qt.AlignLeft, hide_verical_scrollbar=True, *args, **kwargs):
        super(CategoryComboBoxDialog, self).__init__(*args, **kwargs)
        self.setStyleSheet(program_data['manager'].get_stylesheet())
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Popup)
        self.alignment = alignment

        self.layout = QW.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.width = 300
        self.height = 200

        self.options = OrderedDict()
        self.options_list = QW.QListWidget()
        self.layout.addWidget(self.options_list)
        self.ignored_options = []

        self.graphics_view = QW.QGraphicsView()
        self.graphics_scene = QW.QGraphicsScene()
        self.graphics_view.setScene(self.graphics_scene)
        self.layout.addWidget(self.graphics_view)

        self.graphics_view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        if hide_verical_scrollbar:
            self.graphics_view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def connect_signals(self):
        self.rejected.connect(self.close)

    def add_option(self, name, on_click=lambda: None, category=[]):
        self.options[name] = {'on_click': on_click, 'category': category}
        self.update_options()

    def update_options(self):
        self.options_list.clear()
        for option in self.options:
            if option in self.ignored_options: continue
            option_item = QW.QListWidgetItem(option)
            option_item.setTextAlignment(self.alignment)
            self.options_list.addItem(option_item)

    def start(self, pos=None, pos_type='top', ignored_options=[]):
        self.ignored_options = ignored_options
        self.update_options()

        if pos:
            geometry = program_data['window'].geometry()
            bottom = geometry.y() + geometry.height()

            self.setFixedSize(self.width, self.height)

            if pos_type == 'top':
                if pos.y() + self.height > bottom: y = bottom - self.height
                else:                         y = pos.y()
            elif pos_type == 'bottom':
                y = pos.y() - self.height

            self.move(pos.x(), y)

        self.exec_()

    def close(self):
        self.setHidden(True)
        self.accept()
        self.closed.emit()

    def clear(self):
        self.ignored_options = []
        self.options = OrderedDict()
        self.update_options()
