import PyQt5.QtWidgets as QW
from PyQt5.QtCore import Qt, pyqtSignal, QRectF
from PyQt5.QtGui import QCursor

from utils.min_max import min_max
from utils.interpolate import interpolate

from widgets.editors.base_attribute_editor import BaseAttributeEditor
from widgets.rect_widget import RectWidget

from definitions import program_data, COLORS


class RangeIndicator(QW.QGraphicsView):
    def __init__(self, minimum, maximum, show_float=True, *args, **kwargs):
        super(RangeIndicator, self).__init__(*args, **kwargs)
        self.scene = QW.QGraphicsScene()
        self.setScene(self.scene)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.show_float = show_float

        self.background = RectWidget(COLORS['background_light'], 0, 0, 237, 100)
        self.scene.addItem(self.background)

        self.minimum = minimum
        self.maximum = maximum
        self.minimum_label = self.add_label(self.minimum, 0)
        self.maximum_label = self.add_label(self.maximum, 236, True)
        self.active_label  = self.add_label(0, 0)

        self.setContentsMargins(0, 0, 0, 0)

    def move_active(self, value, weight):
        if self.show_float: value = "%.2f" % (value / 100)
        else:               value = str(int(value))
        width = 8 * (len(value))
        x = interpolate(2, 212, weight) - width / 3.5
        if x < 0: x = 0
        real_x = x + width
        if real_x > 230: x -= real_x - 230

        self.active_label.setText(value)
        self.active_label.setMaximumWidth(width)
        self.active_label.move(x, 0)

        if x - 2 < (8 * len(str(self.minimum)))       : self.minimum_label.hide()
        else                                          : self.minimum_label.show()
        if real_x > 227 - (8 * len(str(self.maximum))): self.maximum_label.hide()
        else                                          : self.maximum_label.show()

    def add_label(self, text, x, alignRight=False, color=COLORS["foreground_dark"]):
        width = 8 * (len(str(text)) + 1)
        if alignRight: x -= width
        label = QW.QLabel(f'{text}')
        label.setMaximumWidth(width)
        label.setStyleSheet(f'color:{color};{program_data["manager"].get_font()};background-color:{COLORS["background_light"]}')
        label.setAlignment(Qt.AlignLeft)
        label.move(x, 0)
        self.scene.addWidget(label)
        return label


class SliderAttributeEditor(BaseAttributeEditor):
    def __init__(self, effectData=None, *args, **kwargs):
        super(SliderAttributeEditor, self).__init__(*args, **kwargs)
        self.widget = SliderAttributeEditorWidget(self)
        self.layout().addWidget(self.widget)

        self.indicator_active = False
        if effectData:
            self.indicator_active = True
            show_float = True
            if effectData['type'] == 'int':
                show_float = False
            self.indicator = RangeIndicator(effectData['min'], effectData['max'], show_float=show_float)
            self.layout().addWidget(self.indicator)


class SliderAttributeEditorWidget(QW.QSlider):
    def __init__(self, parent, indicator=False):
        super(SliderAttributeEditorWidget, self).__init__(Qt.Horizontal)
        self.setOrientation(Qt.Horizontal)
        self.parent = parent

    def get_value_from_pos(self, pos):
        return interpolate(self.minimum(), self.maximum(), min_max(pos.x() / self.width(), 0, 1))

    def update_mouse(self, pos):
        self.setValue(self.get_value_from_pos(pos))

    def setValue(self, value):
        super(SliderAttributeEditorWidget, self).setValue(value)
        weight = 0.5
        if self.minimum() == 0:   weight = value / self.maximum()
        elif self.minimum() > 0:  weight = (value + self.minimum()) / (self.maximum() + self.minimum())
        elif self.minimum() < 0:  weight = (value - self.minimum()) / (self.maximum() - self.minimum())
        if self.parent.indicator_active: self.parent.indicator.move_active(value, weight)

    def mousePressEvent  (self, event): self.parent.on_mouse_press  (event.button(), event.pos())
    def mouseMoveEvent   (self, event): self.parent.on_mouse_move   (event.button(), event.pos())
    def mouseReleaseEvent(self, event): self.parent.on_mouse_release(event.button(), event.pos())
