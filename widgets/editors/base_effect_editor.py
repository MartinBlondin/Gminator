from widgets.editors.base_module_editor import BaseModuleEditor
from widgets.category_combo_box import CategoryComboBox

from definitions import program_data, EFFECTS


class BaseEffectEditor(BaseModuleEditor):
    effect_prefix = None

    def __init__(self, parent, effect_id, controller, *args, **kwargs):
        super(BaseEffectEditor, self).__init__(parent, effect_id, controller, *args, **kwargs)

    def module_specific_init_ui(self):
        effect_combo_box = CategoryComboBox(width=150, height=30, pos_type='top')
        self.children['moduleToolbar'].layout().replaceWidget(self.children['moduleComboBox'], effect_combo_box)
        self.children['moduleComboBox'].deleteLater()
        self.children['moduleComboBox'] = effect_combo_box

    def connect_signals(self):
        super(BaseEffectEditor, self).connect_signals()

        self.name_change.connect(program_data['manager'].change_effect_name)
        self.delete_module.connect(program_data['manager'].delete_effect)

        program_data['signals'].effect_name_changed.connect(self.set_name_combo_box)

    def setup_combo_box(self):
        for effect_name, effect in EFFECTS.items():
            self.children['moduleComboBox'].addItem(effect_name.split('.')[1])
        self.children['moduleComboBox'].setCurrentText(program_data['effects'][self.module_id]['name'].split('.')[1])

    def get_attribute_value_at_frame(self, frame, attribute_name):
        effect = program_data['effects'][self.module_id]
        layer_id = effect['layer']
        if (frame in program_data['render_states'] and
            layer_id in program_data['render_states'][frame] and
            self.module_id in program_data['render_states'][frame][layer_id][0]):
            attribute_value = program_data['render_states'][frame][layer_id][0][self.module_id][attribute_name]
            if effect['attributes'][attribute_name]['type'].split(':')[0] == 'position': attribute_value *= 0.01
        else:
            attribute_value = program_data['manager'].get_attribute_value_at_frame(frame, self.module_id, attribute_name)
        return attribute_value

    def on_name_changed(self, effect_name):
        if self.emit_name_change:
            self.name_change.emit(self.module_id, '{}.{}'.format(self.effect_prefix, effect_name))

    def get_attributes(self):
        return program_data['effects'][self.module_id]['attributes']
