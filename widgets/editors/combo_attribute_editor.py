from widgets.combo_box import ComboBox

from widgets.editors.base_attribute_editor import BaseAttributeEditor


class ComboAttributeEditor(BaseAttributeEditor):
    def __init__(self, width, height, *args, **kwargs):
        super(ComboAttributeEditor, self).__init__(*args, **kwargs)
        self.widget = ComboBox(self, width=width, height=height-10)
        self.layout().addWidget(self.widget)
        self.widget.right_clicked.connect(self.start_dialog)

        self.currentIndexChanged = self.widget.currentIndexChanged
        self.addItem             = self.widget.addItem
        self.setCurrentIndex     = self.widget.setCurrentIndex
        self.setObjectName       = self.widget.setObjectName
        self.setValue            = self.setCurrentIndex
