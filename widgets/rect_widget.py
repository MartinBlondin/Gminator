from PyQt5 import QtWidgets as QW
from PyQt5.QtGui import QColor, QPainterPath


class RectWidget(QW.QGraphicsRectItem):
    def __init__(self, color, x, y, width, height, opacity=1, bradius=0, *args, **kwargs):
        super(RectWidget, self).__init__(x, y, width, height, *args, **kwargs)
        self.opacity = opacity
        self.real_opacity = opacity
        self.setColor(color)
        self.bradius = bradius
        self.path = self.get_path()
        if bradius: self.paint_lambda = lambda self, painter: painter.fillPath(self.path, self.qcolor)
        else:       self.paint_lambda = lambda self, painter: painter.fillRect(self.rect(), self.qcolor)

    def get_path(self):
        path = QPainterPath()
        path.addRoundedRect(self.rect(), self.bradius, self.bradius)
        return path

    def set_rect(self, x=None, y=None, width=None, height=None):
        if not x: x = self.rect().x()
        if not y: y = self.rect().y()
        if not width: width = self.rect().width()
        if not height: height = self.rect().height()
        self.setRect(x, y, width, height)
        if self.bradius: self.path = self.get_path()

    def move(self, x, y):
        self.set_rect(x, y)

    def setFixedWidth(self, width):
        self.set_rect(width=width)

    def setFixedHeight(self, height):
        self.set_rect(height=height)

    def setHidden(self, hide):
        if hide: self.real_opacity = 0
        else:    self.real_opacity = self.opacity
        self.setColor('#{}'.format(self.color[3:]))
        self.update()

    def setColor(self, color):
        color = '#{}{}'.format(format(int(self.real_opacity * 255), '02x'), color[1:])
        self.color = color
        self.qcolor = QColor(color)
        self.update()

    def paint(self, painter, *args, **kwargs):
        self.paint_lambda(self, painter)
