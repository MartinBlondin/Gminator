import PyQt5.QtWidgets as QW

from PyQt5.QtCore import pyqtSignal, pyqtSlot

from PIL.ImageQt import ImageQt

from widgets.controllers.base_controller import BaseController
from widgets.qmlComponents.PreviewWidget import Ui_PreviewWidget
from widgets.preview_image import PreviewImage

from definitions import program_data, GRAPHICS_DIR


class PreviewController(BaseController):
    def __init__(self, *args, **kwargs):
        super(PreviewController, self).__init__(*args, **kwargs)

    def init_ui(self):
        Ui_PreviewWidget().setupUi(self)
        self.children = {
            'layerPreviewContainer': self.findChild(QW.QFrame, 'layerPreviewContainer'),
            'mainPreviewContainer': self.findChild(QW.QFrame, 'mainPreviewContainer'),
        }

        self.preview_layer = PreviewImage(self.parent)
        self.children['layerPreviewContainer'].layout().addWidget(self.preview_layer)

        self.preview_merged = PreviewImage(self.parent)
        self.children['mainPreviewContainer'].layout().addWidget(self.preview_merged)

    def connect_signals(self):
        program_data['signals'].preview_changed.connect(self.update_preview)
        program_data['signals'].layer_selected.connect(self.select_layer)
        program_data['signals'].keyframe_selected.connect(self.select_frame)
        program_data['signals'].playback_frame_changed.connect(self.select_frame)

    def select_layer(self, *args, **kwargs):
        if not program_data['playback_playing']: frame = program_data['selected_frame']
        else: frame = program_data['playback_frame']
        maximum = program_data['manager'].get_last_keyframe_for_layer(program_data['selected_layer'])
        if int(frame) > maximum: frame = str(maximum)

        try: self.preview_layer.set_image(ImageQt(program_data['preview_images'][frame][program_data['selected_layer']]['image']))
        except KeyError: self.preview_layer.set_image(None)

    def select_frame(self, frame):
        maximum = program_data['manager'].get_last_keyframe()
        if int(frame) > maximum:
            frame = str(maximum)
        try:
            self.preview_merged.set_image(ImageQt(program_data['preview_images'][frame]['merged']['image']))
        except KeyError:
            self.preview_merged.set_image(None)

        maximum = program_data['manager'].get_last_keyframe_for_layer(program_data['selected_layer'])
        if int(frame) > maximum:
            frame = str(maximum)
        try:
            self.preview_layer.set_image(ImageQt(program_data['preview_images'][frame][program_data['selected_layer']]['image']))
        except KeyError:
            self.preview_layer.set_image(None)

    def update_preview(self, frame, layer_id):
        if frame == program_data['selected_frame'] or int(program_data['selected_frame']) > program_data['manager'].get_last_keyframe():
            if frame in program_data['preview_images']:
                if layer_id == program_data['selected_layer']:
                    self.preview_layer.set_image(ImageQt(program_data['preview_images'][frame][layer_id]['image']))
                elif layer_id == 'merged':
                    self.preview_merged.set_image(ImageQt(program_data['preview_images'][frame][layer_id]['image']))
            else:
                self.preview_layer.set_image(None)
                self.preview_merged.set_image(None)
