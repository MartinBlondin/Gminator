import PyQt5.QtWidgets as QW

from PyQt5.QtCore import pyqtSignal, pyqtSlot

from widgets.controllers.base_controller import BaseController
from widgets.qmlComponents.ToolbarWidget import Ui_ToolbarWidget
from widgets.timeline.timeline_control_buttons import TimelineControlButtons

from definitions import program_data


class ToolbarController(BaseController):
    def __init__(self, *args, **kwargs):
        super(ToolbarController, self).__init__(*args, **kwargs)

    def init_ui(self):
        Ui_ToolbarWidget().setupUi(self)
        self.children = {
            'FPSSpinBox':           self.findChild(QW.QSpinBox, 'FPSSpinBox'),
            'widthSpinBox':         self.findChild(QW.QSpinBox, 'widthSpinBox'),
            'heightSpinBox':         self.findChild(QW.QSpinBox, 'heightSpinBox'),
            'timelineBtnContainer': self.findChild(QW.QHBoxLayout, 'timelineBtnContainer'),
        }

        self.children['timelineBtnContainer'].addWidget(TimelineControlButtons(self.height() - 4))
        self.children['FPSSpinBox'].setValue(program_data['fps'])
        self.children['widthSpinBox'].setValue(program_data['animation_size'][0])
        self.children['heightSpinBox'].setValue(program_data['animation_size'][1])

    def connect_signals(self):
        self.children['FPSSpinBox'].valueChanged.connect(program_data['manager'].set_fps)
        self.children['widthSpinBox'].valueChanged.connect(program_data['manager'].set_animation_width)
        self.children['heightSpinBox'].valueChanged.connect(program_data['manager'].set_animation_height)

        program_data['signals'].fps_changed.connect(self.children['FPSSpinBox'].setValue)
        program_data['signals'].animation_size_changed.connect(self.set_size)

    def set_size(self, width, height):
        self.children['widthSpinBox'].setValue(width)
        self.children['heightSpinBox'].setValue(height)
