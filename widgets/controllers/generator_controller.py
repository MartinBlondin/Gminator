from collections import OrderedDict
from pathlib import Path

import PyQt5.QtWidgets as QW
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from utils.odict_insert_at_index import odict_insert_at_index
from utils.get_path_as_string import get_path_as_string

from widgets.controllers.base_controller import BaseController
from widgets.editors.base_generator_editor import BaseGeneratorEditor

from widgets.icon_button import IconButton

from definitions import program_data, GRAPHICS_DIR

ADD_GENERATOR_ICON = get_path_as_string(Path(GRAPHICS_DIR, 'iconPlus.svg'))


class GeneratorController(BaseController):
    generator_add = pyqtSignal()

    def __init__(self, *args, **kwargs):
        self.generators = OrderedDict()
        self.width = 0
        self.hidden = False

        super(GeneratorController, self).__init__(*args, **kwargs)

    def init_ui(self):
        self.setFixedSize(0, 0)

    def connect_signals(self):
        self.generator_add.connect(program_data['manager'].add_generator)

        program_data['signals'].generator_added.connect(self.add_generator)
        program_data['signals'].generator_deleted.connect(self.delete_generator)

    def add_button_clicked(self):
        self.generator_add.emit()

    @pyqtSlot()
    def refresh_ui(self, *args, **kwargs):
        if not self.hidden:
            for __, generator in self.generators.items():
                generator.hide()

            x_offset = 0
            visible_generators = []
            for generator_id in list(self.generators.keys()):
                if generator_id not in program_data['generators']:
                    self.generators.pop(generator_id).deleteLater()
                    continue
                self.generators[generator_id].move(x_offset, 0)
                self.generators[generator_id].show()
                x_offset += self.generators[generator_id].width()

            self.width = x_offset

            self.parent.refresh_ui()

    def setHidden(self, hidden):
        self.hidden = hidden
        for __, generator in self.generators.items():
            generator.setHidden(hidden)

    @pyqtSlot(str, int)
    def add_generator(self, generator_id, queue_order):
        self.generators = odict_insert_at_index(self.generators, generator_id, BaseGeneratorEditor(self.parent, generator_id, self), queue_order)
        # self.generators[generator_id].setHidden(self.hidden)
        self.refresh_ui()

    @pyqtSlot(str)
    def delete_generator(self, generator_id):
        if generator_id in self.generators:
            self.generators.pop(generator_id).deleteLater()
            self.refresh_ui()
