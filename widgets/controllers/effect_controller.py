from collections import OrderedDict
from pathlib import Path

import PyQt5.QtWidgets as QW
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from utils.odict_insert_at_index import odict_insert_at_index
from utils.get_path_as_string import get_path_as_string

from widgets.controllers.base_controller import BaseController

from widgets.icon_button import IconButton
from widgets.editors.gmic_effect_editor import GmicEffectEditor

from definitions import program_data, GRAPHICS_DIR

ADD_EFFECT_ICON = get_path_as_string(Path(GRAPHICS_DIR, 'iconPlus.svg'))


class EffectController(BaseController):
    effect_add = pyqtSignal(str, str, int)

    def __init__(self, *args, **kwargs):
        self.effects = OrderedDict()
        self.width = 0
        self.hidden = False

        super(EffectController, self).__init__(*args, **kwargs)

    def init_ui(self):
        self.setFixedSize(0, 0)

    def connect_signals(self):
        self.effect_add.connect(program_data['manager'].add_effect)


        program_data['signals'].effect_added.connect(self.add_effect)
        program_data['signals'].effect_deleted.connect(self.delete_effect)

        program_data['signals'].layer_selected.connect(self.refresh_ui)

    def add_button_clicked(self):
        self.effect_add.emit('gmic.Cartoon', program_data['selected_layer'], len(program_data['effects']))

    @pyqtSlot()
    def refresh_ui(self, *args, **kwargs):
        if not self.hidden:
            x_offset = 0
            visible_effects = []
            for effect_id in list(self.effects.keys()):
                if effect_id not in program_data['effects']:
                    self.effects.pop(effect_id).deleteLater()
                    continue
                elif program_data['effects'][effect_id]['layer'] == program_data['selected_layer']:
                    visible_effects.append(effect_id)
                else:
                    self.effects[effect_id].setHidden(True)
            for effect_id in visible_effects:
                self.effects[effect_id].move(x_offset, 0)
                self.effects[effect_id].setHidden(False)
                x_offset += self.effects[effect_id].width()

            self.width = x_offset

            self.parent.refresh_ui()

    def setHidden(self, hidden):
        self.hidden = hidden
        for __, effect in self.effects.items(): effect.setHidden(hidden)

    @pyqtSlot(str, int)
    def add_effect(self, effect_id, queue_order):
        odict_insert_at_index(self.effects, effect_id, GmicEffectEditor(self.parent, effect_id, self), queue_order)
        self.effects[effect_id].setHidden(self.hidden)
        self.refresh_ui()

    @pyqtSlot(str)
    def delete_effect(self, effect_id):
        if effect_id in self.effects:
            self.effects.pop(effect_id).deleteLater()
            self.refresh_ui()
