import PyQt5.QtWidgets as QW
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtGui import QImage, QColor, QPainter
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QRectF, QTimer, QPointF, QPoint

from widgets.rect_widget import RectWidget
from widgets.timeline.timeline_view import TimelineView
from widgets.controllers.base_controller import BaseController
from widgets.timeline.timeline_lines_manager import TimelineLinesManager
from widgets.timeline.timeline_frames_manager import TimelineFramesManager
from widgets.timeline.timeline_animator_widget import TimelineAnimatorWidget
from widgets.timeline.timeline_selection_manager import TimelineSelectionManager

from utils.delay import delay
from utils.copy_dict import copy_dict
from utils.rate_limit import rate_limit
from utils.get_rect_from_pos import get_rect_from_pos
from utils.get_path_as_string import get_path_as_string
from utils.pos_inside_rect import pos_inside_rect

from definitions import program_data, COLORS, GRAPHICS_DIR


class TimelineController(BaseController):
    select_frame = pyqtSignal(str)

    def __init__(self, *args, **kwargs):
        self.frame_distance = 70
        self.frame_offset = 1

        super(TimelineController, self).__init__(*args, **kwargs)

    def init_ui(self):
        self.setLayout(QW.QVBoxLayout())
        self.view = TimelineView(self)
        self.scene = QW.QGraphicsScene()
        self.view.setScene(self.scene)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.layout().addWidget(self.view)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.view.setContentsMargins(0, 0, 0, 0)

        self.width = self.parent.width()
        self.height = self.parent.height()

        self.background = RectWidget(COLORS['background_dark'], 0, 0, self.width * 2, self.height * 2)
        self.scene.addItem(self.background)
        self.visible_area = QRectF(0, 0, 2000, self.height * 2)
        self.drag_indicator = RectWidget(COLORS['timeline_drag_indicator'], 0, 0,
                                         0, 0, opacity=0.04)
        self.drag_indicator.setHidden(True)
        self.scene.addItem(self.drag_indicator)
        self.hide_selected = False

        self.mouse_position = QPointF(0, 0)
        self.hovered_frame = None
        self.last_hovered_frame = False

        self.animators = {}
        self.animator_width = 200
        self.animator_margin = 20
        self.header_height = 20
        self.attribute_height = 40
        self.lines_start_position = self.animator_width + 10
        self.marker_size = 45

        self.x_offset = 4
        self.y_offset = 0

        self.selection = TimelineSelectionManager(self)
        self.frames = TimelineFramesManager(self, self.animator_width)
        self.lines = TimelineLinesManager(self, self.lines_start_position)

        self.update_ui()

    def connect_signals(self):
        self.select_frame.connect(program_data['manager'].select_keyframe)

        program_data['signals'].keyframe_added.connect(self.update_ui)
        program_data['signals'].keyframe_changed.connect(self.update_ui)
        program_data['signals'].keyframe_deleted.connect(self.update_ui)
        program_data['signals'].keyframe_target_added.connect(self.update_ui)
        program_data['signals'].keyframe_target_deleted.connect(self.update_ui)

        program_data['signals'].effect_deleted.connect(self.update_ui)
        program_data['signals'].effect_name_changed.connect(self.update_ui)

        program_data['signals'].layer_selected.connect(self.on_layer_selected_changed)
        program_data['signals'].window_size_changed.connect(self.on_resize)

    def get_x_from_frame(self, frame):
        frame -= self.frame_offset
        x = frame * self.frame_distance
        x += self.lines.x_offset
        return x

    def get_frame_from_pos(self, pos):
        if isinstance(pos, QPoint): pos = self.map_pos_to_scene(pos)
        return int(((pos.x() - self.lines.x_offset) / self.frame_distance)) + self.frame_offset

    def map_pos_to_scene(self, pos):
        return self.view.mapToScene(pos)

    def on_right_mouse_pressed(self):
        if self.view.pressing_right_mouse:
            self.selection.start_dialog(self.view.pressing_right_mouse_start_pos)

    def get_all_markers(self):
        for animator_id, animator in self.animators.items():
            for frame, attribute_name, marker in animator.get_markers():
                if animator.hidden: continue
                if attribute_name == 'Header' and not animator.folded: continue
                elif attribute_name != 'Header' and animator.folded: continue
                if attribute_name in animator.hidden_attributes: continue
                yield frame, animator_id, attribute_name, marker

    def get_all_markers_within_rect(self, rect):
        lowest_frame  = self.get_frame_from_pos(rect.topLeft())
        if rect.width() > self.frame_distance:
            highest_frame = self.get_frame_from_pos(rect.topRight()) + 1
            possible_frames = [str(frame) for frame in range(lowest_frame, highest_frame)]
        else:
            possible_frames = [str(frame) for frame in [lowest_frame - 1, lowest_frame, lowest_frame + 1]]
        for frame, animator_id, attribute_name, marker in self.get_all_markers():
            if frame not in possible_frames: continue
            if self.drag_indicator.rect().contains(marker.center()):
                yield(frame, animator_id, attribute_name)

    def get_marker_from_pos(self, pos):
        if isinstance(pos, QPoint): pos = self.map_pos_to_scene(pos)
        mouse_frame = str(self.get_frame_from_pos(pos))
        for frame, animator_id, attribute_name, marker in self.get_all_markers():
            if frame == mouse_frame and marker.get_rect(QRectF).contains(pos):
                yield frame, animator_id, attribute_name

    def scale(self, velocity):
        self.frame_distance -= velocity * 0.04
        if self.frame_distance < 20:
            self.frame_distance = 20
        if self.frame_distance > 100:
            self.frame_distance = 100
        for animator_id, animator in self.animators.items():
            animator.on_scale()
        self.frames.on_scale()
        self.lines.on_scale()
        self.selection.on_scale()

    def scroll(self, velocity):
        if self.mouse_position.x() > self.lines_start_position:
            new_x = self.x_offset + (velocity * (self.visible_area.width() / 1000))
            minimum = 4
            if new_x < minimum:
                new_x = minimum
            self.x_offset = new_x
            self.frame_offset = int(self.x_offset / self.frame_distance) + 1
            for animator_id, animator in self.animators.items():
                animator.on_scroll()
            self.frames.on_scroll()
            self.lines.on_scroll()
            self.selection.on_scroll()
        else:
            velocity *= 1.2
            new_y = self.y_offset - (velocity * (self.visible_area.height() / 400))
            maximum = 0
            if new_y > maximum:
                new_y = maximum
            minimum = 0
            for animator_id, animator in self.animators.items():
                minimum += animator.height()
            minimum -= self.attribute_height
            minimum *= -1
            if new_y < minimum:
                new_y = minimum
            self.y_offset = new_y
            animator_y_offset = self.y_offset + self.animator_margin
            for animator_id, animator in self.animators.items():
                if animator.hidden: continue
                animator.set_y_offset(animator_y_offset)
                animator_y_offset += animator.height() + self.animator_margin
            self.selection.on_scroll()

    def update_drag(self):
        if self.view.dragging:
            self.drag_indicator.setHidden(False)
        else:
            self.drag_indicator.setHidden(True)

    def update_move(self):
        if self.view.moving:
            self.selection.copy_selection_markers()
            self.selection.move_selection_markers()
        else:
            self.selection.clear_selection_markers()

    def frame_clicked(self, frame):
        if int(frame) > 0:
            self.select_frame.emit(str(frame))

    def on_shift_pressed(self):
        if self.view.moving or self.view.scaling:
            for frame, animator_id, attribute_name in self.selection.get_selection():
                if attribute_name == 'Header':
                    marker = self.animators[animator_id].header_markers[frame]
                else:
                    marker = self.animators[animator_id].attribute_markers[frame][attribute_name]

                if (self.view.pressing_shift or frame == '1') and not self.view.scaling:
                    if not self.animators[animator_id].hidden:
                        marker.setHidden(False)
                        self.hide_selected = False
                else:
                    marker.setHidden(True)
                    self.hide_selected = True

    def update_mouse_position(self, pos, frame):
        self.mouse_position = pos
        self.hovered_frame = int(frame)

        if self.view.moving:
            if not self.hovered_frame == self.last_hovered_frame:
                self.selection.move_selection_markers()
        else:
            if not self.hovered_frame == self.last_hovered_frame:
                if self.mouse_position.x() > self.lines_start_position and pos_inside_rect(self.mouse_position, self.visible_area, margin=-60):
                    self.lines.hover(self.hovered_frame)
                else:
                    self.hovered_frame = None
                    self.lines.hover(None)

            if self.view.dragging:
                rect = get_rect_from_pos(self.mouse_position, self.view.drag_start_pos, QRectF).intersected(self.visible_area)
                self.drag_indicator.setRect(rect)

        self.last_hovered_frame = self.hovered_frame

    def on_layer_selected_changed(self):
        self.selection.clear_selection()
        effects = program_data['manager'].get_effects_in_layer(program_data['selected_layer'])
        for animator_id, animator in self.animators.items():
            if animator_id in effects:
                self.animators[animator_id].update_attributes()
            else:
                self.animators[animator_id].update_hidden()

    def update_ui(self):
        animator_y_offset = self.y_offset + self.animator_margin
        effects = program_data['manager'].get_effects_in_layer(program_data['selected_layer'])
        for animator_id in effects.keys():
            if animator_id not in self.animators.keys():
                self.animators[animator_id] = TimelineAnimatorWidget(self, animator_id, effects[animator_id]['name'].split('.')[1],
                                                                     4, animator_y_offset, self.animator_width)
                self.scene.addWidget(self.animators[animator_id])
            self.animators[animator_id].set_name(effects[animator_id]['name'].split('.')[1])
            self.animators[animator_id].set_y_offset(animator_y_offset)
            self.animators[animator_id].update_attributes()
            if self.animators[animator_id].hidden: continue
            animator_y_offset += self.animators[animator_id].height() + self.animator_margin

        for animator_id in list(self.animators.keys()):
            if animator_id not in program_data['effects'].keys():
                for frame, selected_animator_id, attribute_name in list(self.selection.get_selection()):
                    if selected_animator_id == animator_id:
                        self.selection.deselect(frame, animator_id, attribute_name)
                self.animators.pop(animator_id).deleteLater()
            elif not animator_id in effects.keys():
                self.animators[animator_id].setHidden(True)
            elif not self.animators[animator_id].hidden:
                self.animators[animator_id].setHidden(False)

        self.view.fitInView(self.visible_area)

    def on_resize(self):
        self.view.fitInView(self.visible_area)
