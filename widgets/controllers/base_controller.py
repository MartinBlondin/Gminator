import PyQt5.QtWidgets as QW


class BaseController(QW.QWidget):
    def __init__(self, parent, *args, **kwargs):
        super(BaseController, self).__init__(*args, **kwargs)

        self.parent = parent
        self.init_ui()
        self.connect_signals()

    def init_ui(self):
        pass

    def connect_signals(self):
        pass
