import os
import numpy as np

from pathlib import Path
from collections import OrderedDict
from blend_modes import blend_modes
from PIL import Image

from utils.get_path_as_string import get_path_as_string

import json

# Flags
#------------------------------------------------------------------------------------#
DEBUG = True                                                                         #
#------------------------------------------------------------------------------------#


# ints
#------------------------------------------------------------------------------------#
MAX_RENDER_THREADS        = 4    # virtual threads, can be unlimited but be careful  #
RENDER_STATE_UPDATE_DELAY = 400  # in milliseconds, makes the previews react faster  #
                                                                                     #
DEFAULT_FPS               = 12                                                       #
DEFAULT_ANIMATION_SIZE    = (640, 360)                                               #
PREVIEW_SCALE_FACTOR      = 0.5                                                      #
HISTORY_MAX_CHANGE_AMOUNT = 15                                                       #
#------------------------------------------------------------------------------------#


# Paths
#------------------------------------------------------------------------------------#
ROOT_DIR = Path(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

STATIC_DIR   = Path(ROOT_DIR,   'static')
QSS_DIR      = Path(STATIC_DIR, 'qss')
QML_DIR      = Path(STATIC_DIR, 'qml')
GRAPHICS_DIR = Path(STATIC_DIR, 'graphics')

JSON_DIR          = Path(STATIC_DIR, 'json')
EFFECTS_JSON_DIR  = Path(JSON_DIR,   'effects')
SIGNALS_JSON_PATH = Path(JSON_DIR,   'signals.json')

WIDGETS_DIR      = Path(ROOT_DIR,    'widgets')
RENDERED_QML_DIR = Path(WIDGETS_DIR, 'qmlComponents')

PLUS_ICON_PATH              = get_path_as_string(GRAPHICS_DIR, 'iconPlus.svg')
MINUS_ICON_PATH             = get_path_as_string(GRAPHICS_DIR, 'iconMinus.svg')
DELETE_ICON_PATH            = get_path_as_string(GRAPHICS_DIR, 'iconDelete.svg')
SINE_ICON_PATH              = get_path_as_string(GRAPHICS_DIR, 'iconSine.svg')
DOWN_ICON_PATH              = get_path_as_string(GRAPHICS_DIR, 'iconDown.svg')
KEYFRAME_ICON_PATH          = get_path_as_string(GRAPHICS_DIR, 'keyframeMarker.svg')
KEYFRAME_SELECTED_ICON_PATH = get_path_as_string(GRAPHICS_DIR, 'keyframeMarkerSelected.svg')
KEYFRAME_EMPTY_ICON_PATH    = get_path_as_string(GRAPHICS_DIR, 'keyframeMarkerEmpty.svg')

POSITION_EDITOR_CIRCLE_PATH    = get_path_as_string(GRAPHICS_DIR, 'positionEditorCircle.svg')
POSITION_EDITOR_HIGHLIGHT_PATH = get_path_as_string(GRAPHICS_DIR, 'positionEditorHighlight.svg')
POSITION_EDITOR_HOVER_PATH     = get_path_as_string(GRAPHICS_DIR, 'positionEditorHover.svg')

GRAPHICS_LOADING_IMAGE = Image.open(get_path_as_string(GRAPHICS_DIR, 'loading.png'))
#------------------------------------------------------------------------------------#


# Dicts
#------------------------------------------------------------------------------------#
COLORS = {
    'transparent': '#00000000',

    'background_light': '#31363b',
    'background_dark':  '#232629',
    'foreground_light': '#eff0f1',
    'foreground_dark':  '#76797C',
    'accent':           '#00ccff',

    'timeline_line_light':            '#4d4d4d',
    'timeline_line_dark':             '#333333',
    'timeline_line_selected':         '#00ccff',
    'timeline_line_playhead':         '#800000',
    'timeline_frame_tint_complete':   '#336d27',
    'timeline_frame_tint_incomplete': '#b9c149',
    'timeline_drag_indicator':        '#fff8df',

    'keyframe_marker': '#b3b3b3',

    'position_editor_background': '#26292c',

    'button_background': '#232629',
    'button_hover':      '#26292b',
    'button_outline':    '#1d1f21',
    'button_disabled':   '#2a2e33',
}

SHORTCUTS = {
    'new':     'Ctrl+N',
    'save':    'Ctrl+S',
    'save as': 'Ctrl+Shift+S',
    'load':    'Ctrl+O',
    'undo':    'Ctrl+Z',
    'redo':    'Ctrl+Shift+Z',
    'exit':    None,

    'play / pause': 'Space',
    'stop':         'Escape',
    'next':         None,
    'previous':     None,
    'last':         None,
    'first':        None,
}

EFFECTS = {}
EFFECT_CATEGORIES = {}
EFFECT_COMMANDS = {}

def add_effect(json_dict, prefix):
    for effect_name, effect in json_dict['effects'].items(): EFFECTS['{}.{}'.format(prefix, effect_name)] = effect
    for category_name, effect_list in json_dict['categories'].items(): EFFECT_CATEGORIES['{}.{}'.format(prefix, category_name)] = effect_list
    EFFECT_COMMANDS[prefix] = json_dict['commands']
for json_path in EFFECTS_JSON_DIR.iterdir():
    if json_path.suffix == '.json':
        with Path(json_path).open() as f: add_effect(json.loads(f.read()), json_path.parts[-1].split('.')[0])

GENERATOR_SIGNALS = {}
with SIGNALS_JSON_PATH.open() as f:
    signals = json.loads(f.read())
    for key in signals.keys():
        GENERATOR_SIGNALS[key] = {}
        for name, data in signals[key].items():
            if name == 'Shared': GENERATOR_SIGNALS[key]['attributes'] = data['attributes']
            else:                GENERATOR_SIGNALS[key][name] = data

# absolutely disgusting
BM_BLEND_MODE_COMMAND = lambda root_image, layer_image, opacity, bm_command: Image.fromarray(np.uint8(bm_command(np.asarray(root_image, np.float), np.asarray(layer_image, np.float), opacity))).convert('RGBA')

BLEND_MODES = {'normal':        lambda root_image, layer_image, opacity: Image.blend(root_image, layer_image, opacity),
               'darken':        lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.darken_only),
               'lighten':       lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.lighten_only),
               'soft light':    lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.soft_light),
               'hard light':    lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.hard_light),
               'dodge':         lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.dodge),
               'addition':      lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.addition),
               'multiply':      lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.multiply),
               'subtract':      lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.subtract),
               'divide':        lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.divide),
               'difference':    lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.difference),
               'grain extract': lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.grain_extract),
               'grain merge':   lambda root_image, layer_image, opacity: BM_BLEND_MODE_COMMAND(root_image, layer_image, opacity, blend_modes.grain_merge),
}

LAYER_TEMPLATE = {
    'name':       'Root',
    'blend_mode': 'normal',
    'opacity':    100,
    'input_mode': None,
    'input_path': None,
}
EFFECT_TEMPLATE = {
    'layer':      'root',
    'name':       None,
    'command':    None,
    'attributes': {},
    'flags':      '',
}
GENERATOR_TEMPLATE = {
    'name':        None,
    'type':        None,
    'attributes':  {},
    'connections': {},
}

# Controlled by backend/DataManager.py
program_data = {
    'manager': None,
    'signals': None,

    'window':       None,
    'window_size': (None, None),
    'logger':       None,

    'animation_size': DEFAULT_ANIMATION_SIZE,
    'fps':            DEFAULT_FPS,

    'selected_layer':      None,
    'selected_layer_full': None,  # links to program_data[program_data['selected_layer']]
    'selected_frame':      None,

    'playback_playing': False,
    'playback_frame':   1,

    'layers':     OrderedDict(),
    'effects':    OrderedDict(),
    'generators': OrderedDict(),

    'keyframes':               {},
    'keyframe_animator_types': {},
    'keyframe_targets':        {},

    'render_states':  {},
    'preview_images': {},

    'history':        {},
    'history_order':  [],
    'current_change': None,

    'last_save_file_name': None,
    'last_save_on_change': None,

    'event_handlers': {},

    'stats':          {'function_calls': []},
}
#------------------------------------------------------------------------------------#
