from PyQt5.QtCore import QObject, pyqtSignal

from backend.signals import Signals

from definitions import program_data, EFFECTS

class BaseEventHandler(QObject):
    def __init__(self, parent):
        super(BaseEventHandler, self).__init__()
        self.parent = parent
        self.signals = {}
        self.ignored_signals = {}
        if not hasattr(self, 'blocked_signals'): self.blocked_signals = []
        for name, signal in Signals.__dict__.items():
            if name in self.blocked_signals: continue
            if isinstance(signal, pyqtSignal):
                function = 'on_' + name
                self.signals[function] = eval('program_data["signals"].{}'.format(name))

        on_event = hasattr(self, 'on_event')
        for function, signal in self.signals.items():
            if hasattr(self, function):
                signal.connect(eval('self.{}'.format(function)))
            elif on_event:
                signal.connect(self.on_event)

    def add_ignored_signal(self, signal, count):
        self.ignored_signals[signal] = count

    def catch_ignored_signal(self, signal):
        if signal not in self.ignored_signals:
            return True
        elif self.ignored_signals[signal] > 0:
            self.ignored_signals[signal] -= 1
            return False
        else:
            self.ignored_signals.pop(signal)
            return True
