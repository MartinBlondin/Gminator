from backend.event_handlers.base_event_handler import BaseEventHandler

from definitions import program_data, GRAPHICS_LOADING_IMAGE


class PreviewEventHandler(BaseEventHandler):
    def __init__(self, parent):
        super(PreviewEventHandler, self).__init__(parent)

    def on_render_state_changed(self, frame, layer_id, render_state):
        if not render_state:
            if frame in program_data['preview_images'] and layer_id in program_data['preview_images'][frame]:
                if program_data['preview_images'][frame][layer_id]['renderer']: program_data['preview_images'][frame][layer_id]['renderer'].stop()
                program_data['preview_images'].pop(frame)
                program_data['manager'].emit(program_data['signals'].preview_changed, [frame, None])
        else:
            self.parent.start_preview_render(frame, layer_id, render_state)

    def on_layer_input_path_changed(self, layer_id, input_mode, input_path):
        if input_path == None:
            if list(program_data['preview_images']) == ['1']:
                program_data['preview_images']['1'][layer_id]['status'] = 'incomplete'
                program_data['preview_images']['1'][layer_id]['image'] = GRAPHICS_LOADING_IMAGE
                program_data['preview_images']['1']['merged']['status'] = 'incomplete'
                program_data['preview_images']['1']['merged']['image'] = GRAPHICS_LOADING_IMAGE
                frame = program_data['selected_frame']
                program_data['manager'].emit(program_data['signals'].preview_changed, [frame, layer_id])
                program_data['manager'].emit(program_data['signals'].preview_changed, [frame, 'merged'])

    def on_keyframe_deleted(self):
        if not self.catch_ignored_signal('keyframe_deleted'): return
        last_keyframe = program_data['manager'].get_last_keyframe()
        for frame in list(program_data['preview_images']):
            if int(frame) > last_keyframe:
                for layer_id in program_data['layers']:
                    if layer_id in program_data['preview_images'][frame] and program_data['preview_images'][frame][layer_id]['renderer']:
                        program_data['preview_images'][frame][layer_id]['renderer'].stop()
                program_data['preview_images'].pop(frame)
                program_data['manager'].emit(program_data['signals'].preview_changed, [frame, None])
        for layer_id in program_data['layers']:
            last_keyframe_for_layer = program_data['manager'].get_last_keyframe_for_layer(layer_id)
            for frame in list(program_data['preview_images']):
                if int(frame) > last_keyframe_for_layer and layer_id in program_data['preview_images'][frame]:
                    if program_data['preview_images'][frame][layer_id]['renderer']:
                        program_data['preview_images'][frame][layer_id]['renderer'].stop()
                    program_data['preview_images'][frame].pop(layer_id)
                    program_data['manager'].emit(program_data['signals'].preview_changed, [frame, layer_id])

    def on_keyframes_deleting(self, count):
        self.add_ignored_signal('keyframe_deleted', count - 1)
