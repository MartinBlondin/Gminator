from collections import OrderedDict

from backend.managers.data_manager import manager_function_decorator

from utils.odict_insert_at_index import odict_insert_at_index
from utils.copy_dict import copy_dict
from utils.verify import verify

from definitions import program_data, EFFECT_TEMPLATE, EFFECTS


class EffectManagerMixin:
    def __init__(self):
        super(EffectManagerMixin, self).__init__()
        program_data['signals'].layer_deleted.connect(self.delete_effects_in_layer)
        program_data['signals'].clear.connect(self.on_clear)

    @manager_function_decorator
    def add_effect(self, effect_name, layer_id, queue_order=None, preset_id=None):
        effect = copy_dict(EFFECT_TEMPLATE)
        effect['layer'] = layer_id
        effect['name'] = effect_name
        effect.update(EFFECTS[effect_name])

        if preset_id: effect_id = preset_id
        else:         effect_id = self.get_new_id()

        if not queue_order:                queue_order = len(program_data['effects'])
        elif isinstance(queue_order, str): queue_order = int(queue_order)

        odict_insert_at_index(program_data['effects'], effect_id, effect, queue_order)

        to_add = []
        for attribute_name, attribute in effect['attributes'].items():
            to_add.append(['1', effect_id, attribute_name, attribute['value'], 'effect'])
        program_data['manager'].add_keyframes(to_add, emit=False)

        program_data['manager'].emit(program_data['signals'].effect_added, [effect_id, queue_order])
        program_data['manager'].emit(program_data['signals'].keyframes_adding, [len(effect['attributes'])])
        for attribute_name, attribute in effect['attributes'].items():
            program_data['manager'].emit(program_data['signals'].keyframe_added, ['1', effect_id, attribute_name, attribute['value'], 'effect'])

    @manager_function_decorator
    def delete_effect(self, effect_id, *args, **kwargs):
        effect = program_data['effects'][effect_id]
        program_data['effects'].pop(effect_id)

        keyframes = copy_dict(program_data['manager'].get_keyframes(effect_id))
        to_delete = []
        for frame in keyframes:
            to_delete.append([frame, effect_id, None])
        count = program_data['manager'].delete_keyframes(to_delete, emit=False)

        program_data['manager'].emit(program_data['signals'].effect_deleted, [effect_id, effect['name'], effect['layer']])
        program_data['manager'].emit(program_data['signals'].keyframes_deleting, [count])

        for frame in keyframes:
            for attribute_name in keyframes[frame][effect_id]:
                program_data['manager'].emit(program_data['signals'].keyframe_deleted,
                                             [frame, effect_id, attribute_name,
                                              keyframes[frame][effect_id][attribute_name], 'effect'])

    @manager_function_decorator
    def change_effect_name(self, effect_id, effect_name):
        old_name = program_data['effects'][effect_id]['name']
        program_data['effects'][effect_id]['name'] = effect_name
        program_data['effects'][effect_id].update(EFFECTS[effect_name])

        to_delete = []
        for frame, animator_id, attribute_name, __ in program_data['manager'].get_keyframes_generator(effect_id):
            to_delete.append([frame, animator_id, attribute_name])

        to_add = []
        new_attribute_count = 0
        for attribute_name, attribute in EFFECTS[effect_name]['attributes'].items():
            to_add.append(['1', effect_id, attribute_name, attribute['value'], 'effect'])
            new_attribute_count += 1

        if effect_id in program_data['keyframe_targets']:
            program_data['keyframe_targets'].pop(effect_id)

        program_data['signals'].effect_name_changed.emit(effect_id, effect_name, old_name, new_attribute_count)

        program_data['manager'].delete_keyframes(to_delete)
        program_data['manager'].add_keyframes(to_add)

    @manager_function_decorator
    def delete_effects_in_layer(self, layer_id, *args, **kwargs):
        for effect_id in self.get_effects_in_layer(layer_id):
            self.delete_effect(effect_id)

    @manager_function_decorator
    def get_effects_in_layer(self, layer_id):
        result = OrderedDict()
        for effect_id, effect in program_data['effects'].items():
            if effect['layer'] == layer_id:
                result[effect_id] = effect
        return result

    @manager_function_decorator
    def on_clear(self):
        effects = list(program_data['effects'].keys())
        program_data['effects'] = OrderedDict()
        for effect_id in effects:
            program_data['signals'].effect_deleted.emit(effect_id)
