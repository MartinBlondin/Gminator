from PyQt5.QtCore import pyqtSlot

from definitions import program_data

from utils.verify import verify
from utils.delay import delay


class PlaybackManagerMixin:
    def __init__(self, *args, **kwargs):
        super(PlaybackManagerMixin, self).__init__(*args, **kwargs)
        self.playback_delay = None
        program_data['signals'].keyframe_selected.connect(self.set_playback_frame)

    def start_playback(self):
        program_data['playback_playing'] = True
        program_data['playback_frame'] = program_data['selected_frame']
        self.playback_delay = delay(self.progress_playback, 1 / program_data['fps'] * 1000)

        program_data['signals'].playback_playing_changed.emit(program_data['playback_playing'])
        program_data['signals'].playback_frame_changed.emit(program_data['playback_frame'])

    def pause_playback(self):
        program_data['playback_playing'] = False
        program_data['signals'].playback_playing_changed.emit(program_data['playback_playing'])
        # program_data['manager'].select_keyframe(program_data['playback_frame'])

    def stop_playback(self):
        program_data['playback_playing'] = False
        program_data['signals'].playback_playing_changed.emit(program_data['playback_playing'])
        program_data['manager'].select_keyframe('1')

    def set_playback_frame(self, frame):
        if program_data['playback_playing']:
            program_data['playback_frame'] = frame
            program_data['signals'].playback_frame_changed.emit(program_data['playback_frame'])

    def progress_playback(self):
        if program_data['playback_playing']:
            program_data['playback_frame'] = str(int(program_data['playback_frame']) + 1)
            if int(program_data['playback_frame']) > program_data['manager'].get_last_keyframe():
                program_data['playback_frame'] = '1'
            self.playback_delay = delay(self.progress_playback, 1 / program_data['fps'] * 1000)

            program_data['signals'].playback_frame_changed.emit(program_data['playback_frame'])
