from backend.managers.data_manager import manager_function_decorator

from utils.copy_dict import copy_dict
from utils.verify import verify
from utils.dict_place_or_create import dict_place_or_create

from definitions import program_data


class KeyframeManagerMixin:
    def __init__(self):
        self.select_keyframe('1')
        super(KeyframeManagerMixin, self).__init__()
        program_data['signals'].clear.connect(self.on_clear)

    @manager_function_decorator
    def add_keyframe(self, frame, animator_id, attribute_name, attribute_value, animator_type='effect', emit=True):
        if animator_type == 'effect':
            if program_data['effects'][animator_id]['attributes'][attribute_name]['type'].split(':')[0] == 'position' and(
                    isinstance(attribute_value, tuple)):
                link_attribute_name = program_data['effects'][animator_id]['attributes'][attribute_name]['link']
                link_attribute_value = attribute_value[1]
                attribute_value = attribute_value[0]
                to_add = [[frame, animator_id, attribute_name,      attribute_value,      animator_type],
                          [frame, animator_id, link_attribute_name, link_attribute_value, animator_type]]
                self.add_keyframes(to_add, emit)
                return

        added = False
        if frame not in program_data['keyframes'].keys():
            added = True
        elif animator_id not in program_data['keyframes'][frame].keys():
            added = True
        elif attribute_name not in program_data['keyframes'][frame][animator_id].keys():
            added = True

        if not added:
            old_attribute_value = program_data['keyframes'][frame][animator_id][attribute_name]

        dict_place_or_create(program_data['keyframes'], frame, animator_id, attribute_name, attribute_value)

        program_data['keyframe_animator_types'][animator_id] = animator_type

        if emit:
            if added:
                program_data['signals'].keyframe_added.emit(frame, animator_id, attribute_name, attribute_value, animator_type)
            else:
                program_data['signals'].keyframe_changed.emit(frame, animator_id, attribute_name, attribute_value, old_attribute_value)

    @manager_function_decorator
    def add_keyframes(self, keyframes, emit=True):
        if emit:
            program_data['signals'].keyframes_adding.emit(len(keyframes))
        for frame, animator_id, attribute_name, attribute_value, animator_type in keyframes:
            self.add_keyframe(frame, animator_id, attribute_name, attribute_value, animator_type, emit)

    @manager_function_decorator
    def add_keyframe_target(self, animator_id, attribute_name):
        if animator_id not in program_data['keyframe_targets']: program_data['keyframe_targets'][animator_id] = {}
        program_data['keyframe_targets'][animator_id][attribute_name] = {}
        program_data['signals'].keyframe_target_added.emit(animator_id, attribute_name)

    @manager_function_decorator
    def delete_keyframe_target(self, animator_id, attribute_name=None):
        if attribute_name:
            program_data['keyframe_targets'][animator_id].pop(attribute_name)
            if not program_data['keyframe_targets'][animator_id]:
                program_data['keyframe_targets'].pop(animator_id)
        else:
            program_data['keyframe_targets'].pop(animator_id)
        program_data['signals'].keyframe_target_deleted.emit(animator_id, attribute_name)

    @manager_function_decorator
    def delete_keyframes(self, keyframes, emit=True):
        count = len(keyframes)
        for frame, animator_id, attribute_name in keyframes:
            if animator_id in program_data['keyframes'][frame] and attribute_name == None:
                count += len(program_data['keyframes'][frame][animator_id]) - 1
        if emit:
            program_data['manager'].emit(program_data['signals'].keyframes_deleting, [count])
        for frame, animator_id, attribute_name in keyframes:
            self.delete_keyframe(frame, animator_id, attribute_name, emit)
        return count

    @manager_function_decorator
    def delete_keyframe(self, frame, animator_id=None, attribute_name=None, emit=True):
        keyframes = self.get_keyframes()
        attribute_value = None
        animator_type   = None
        if attribute_name:
            attribute_value = program_data['keyframes'][frame][animator_id][attribute_name]
            animator_type = program_data['keyframe_animator_types'][animator_id]
            program_data['keyframes'][frame][animator_id].pop(attribute_name)
            if not program_data['keyframes'][frame][animator_id]: program_data['keyframes'][frame].pop(animator_id)
            if not program_data['keyframes'][frame]: program_data['keyframes'].pop(frame)

        elif animator_id:
            for animator in list(keyframes[frame]):
                if animator == animator_id:
                    for name in list(keyframes[frame][animator_id]):
                        self.delete_keyframe(frame, animator_id, name, emit)
            emit = False
            animator_type = program_data['keyframe_animator_types'][animator_id]

        else:
            for frame, animators in keyframes.items():
                for animator, attributes in animators.items():
                    for name, value in attributes.items():
                        self.delete_keyframe(frame, animator, name, emit)
            emit = False

        if emit:
            program_data['signals'].keyframe_deleted.emit(frame, animator_id, attribute_name, attribute_value, animator_type)

    @manager_function_decorator
    def select_keyframe(self, frame):
        program_data['selected_frame'] = frame
        program_data['signals'].keyframe_selected.emit(frame)

    @manager_function_decorator
    def move_keyframe(self, frame, new_frame, animator_id=None, attribute_name=None, delete=True, emit=True):
        if frame == '1':
            delete = False

        program_data['signals'].keyframe_moving.emit(frame, new_frame, animator_id, attribute_name, delete)
        if attribute_name:
            attribute_value = program_data['keyframes'][frame][animator_id][attribute_name]
            self.add_keyframe(new_frame, animator_id, attribute_name, attribute_value, emit=emit)
        elif animator_id:
            for attr_name in list(program_data['keyframes'][frame][animator_id]):
                self.move_keyframe(frame, new_frame, animator_id, attr_name, delete, emit)
            delete = False
        else:
            for animator_id in list(program_data['keyframes'][frame]):
                self.move_keyframe(frame, new_frame, animator_id, None, delete, emit)
            delete = False

        if delete:
            self.delete_keyframe(frame, animator_id, attribute_name, emit=emit)

    @manager_function_decorator
    def move_keyframes(self, keyframes, emit=True):
        count = len(keyframes)
        for frame, new_frame, animator_id, attribute_name, delete, in keyframes:
            if attribute_name == None:
                count += len(program_data['keyframes'][frame][animator_id])
        program_data['signals'].keyframes_moving.emit(count)
        for frame, new_frame, animator_id, attribute_name, delete in keyframes:
            self.move_keyframe(frame, new_frame, animator_id, attribute_name, delete, emit)

    @manager_function_decorator
    def get_last_keyframe(self):
        if program_data['keyframes']:
            return max(map(int, list(self.get_keyframes())))
        else:
            return 1

    @manager_function_decorator
    def get_last_keyframe_for_layer(self, layer_id):
        last_keyframe = 1
        effects_in_layer = program_data['manager'].get_effects_in_layer(layer_id)
        if program_data['layers'][layer_id]['input_mode'] == 'Clone':
            related_layer_id = program_data['layers'][layer_id]['input_path']
            related_layer_last_keyframe = self.get_last_keyframe_for_layer(related_layer_id)
            if related_layer_last_keyframe > last_keyframe:
                return related_layer_last_keyframe
        else:
            for frame, animators in program_data['keyframes'].items():
                frame = int(frame)
                for animator_id, attributes in animators.items():
                    if animator_id in effects_in_layer:
                        if frame > last_keyframe:
                            last_keyframe = frame

        # also check generators

        return last_keyframe

    @manager_function_decorator
    def get_keyframes_generator(self, target_animator_id=None, animator_type=None):
        for frame, animators in program_data['keyframes'].items():
            for animator_id, attributes in animators.items():
                if target_animator_id and animator_id != target_animator_id: continue
                if animator_type and program_data['keyframe_animator_types'][animator_id] != animator_type: continue
                for attribute_name, attribute_value in attributes.items():
                    if frame != '1' and (animator_id not in program_data['keyframe_targets'] or
                                         attribute_name not in program_data['keyframe_targets'][animator_id]):
                        continue
                    yield(frame, animator_id, attribute_name, attribute_value)

    @manager_function_decorator
    def get_keyframes(self, target_animator_id=None, animator_type=None):
        result = {}
        for frame, animators in program_data['keyframes'].items():
            if frame == '1':
                if target_animator_id: animators = {target_animator_id: animators[target_animator_id]}
                result[frame] = animators
            else:
                for animator_id, attributes in program_data['keyframe_targets'].items():
                    if target_animator_id and animator_id != target_animator_id: continue
                    if animator_type and program_data['keyframe_animator_types'][animator_id] != animator_type: continue
                    if animator_id in animators:
                        if frame not in result: result[frame] = {}
                        result[frame][animator_id] = {}
                        for attribute_name in attributes:
                            if attribute_name in program_data['keyframes'][frame][animator_id]:
                                result[frame][animator_id][attribute_name] = program_data['keyframes'][frame][animator_id][attribute_name]
        return result

    @manager_function_decorator
    def get_amount_of_frames(self, target_animator_id):
        size = 0
        for frame, animator_id, attribute_name, attribute_value in self.get_keyframes_generator():
            if animator_id == target_animator_id:
                size += 1
        return size

    @manager_function_decorator
    def get_all_frames(self):
        for frame in range(program_data['manager'].get_last_keyframe()):
            yield frame + 1

    def on_clear(self):
        keyframes = list(self.get_keyframes_generator())
        program_data['keyframes'] = {}
        program_data['keyframe_animator_types'] = {}
        program_data['manager'].select_keyframe('1')
        for frame, animator_id, attribute_name, attribute_value in keyframes:
            program_data['signals'].keyframe_deleted.emit(frame, animator_id, attribute_name)
