from PyQt5.QtGui import QImage

from collections import OrderedDict

from utils.verify import verify
from backend.event_handlers.history_event_handler import HistoryEventHandler

from definitions import program_data, EFFECT_COMMANDS, HISTORY_MAX_CHANGE_AMOUNT

class HistoryManagerMixin:
    def __init__(self):
        super(HistoryManagerMixin, self).__init__()
        self.functions = {'add_effect':             program_data['manager'].add_effect,
                          'delete_effect':          program_data['manager'].delete_effect,
                          'add_keyframe':           program_data['manager'].add_keyframe,
                          'delete_keyframe':        program_data['manager'].delete_keyframe,
                          'add_layer':              program_data['manager'].add_layer,
                          'delete_layer':           program_data['manager'].delete_layer,
                          'add_keyframe_target':    program_data['manager'].add_keyframe_target,
                          'delete_keyframe_target': program_data['manager'].delete_keyframe_target,

                          'change_effect_name':      program_data['manager'].change_effect_name,
                          'change_keyframe_value':   program_data['manager'].add_keyframe,
                          'change_layer_input_path': program_data['manager'].change_layer_input_path,

                          'side_effect_keyframes_deleted': program_data['manager'].delete_keyframes,
                          'side_effect_keyframes_added':   program_data['manager'].add_keyframes,
        }
        self.args = {'add_effect':      ['effect_name', 'layer_id', 'queue_order', 'effect_id'],
                     'delete_effect':   ['effect_id'],
                     'add_keyframe':    ['frame', 'animator_id', 'attribute_name', 'attribute_value', 'animator_type'],
                     'delete_keyframe': ['frame', 'animator_id', 'attribute_name'],
                     'add_layer':       ['layer_name', 'queue_order', 'layer_id'],
                     'delete_layer':    ['layer_id'],
                     'add_keyframe_target':   ['animator_id', 'attribute_name'],
                     'delete_keyframe_target': ['animator_id', 'attribute_name'],

                     'change_effect_name':      ['effect_id', 'effect_name'],
                     'change_keyframe_value':   ['frame', 'animator_id', 'attribute_name', 'attribute_value', 'animator_type'],
                     'change_layer_input_path': ['layer_id', 'input_mode', 'input_path'],
        }

        program_data['manager'].add_event_handler('history', HistoryEventHandler, self)

        program_data['signals'].clear.connect(self.on_clear)

        self.side_effects = {}

        self.change_id_counter = 0

    def get_change_id(self):
        self.change_id_counter += 1
        return str(self.change_id_counter)

    def register_change(self, change_type, args={}, reverse_args=None):
        change = {'type': change_type,
                  'args': args,
                  'reverse_args': reverse_args,
                  'side_effects': [],
                  'reversed': False}
        if change_type in self.side_effects:
            self.register_side_effect(change_type, change)
        else:
            self.on_change(change)
        return change

    def register_side_effect(self, change_type, side_effect_change):
        side_effect = self.side_effects[change_type]

        if side_effect['link']:
            self.side_effects[side_effect['link']]['counter'] -= 1
            if self.side_effects[side_effect['link']]['counter'] == 0:
                self.remove_side_effect_tracker(change_type)

        else:
            side_effect['counter'] -= 1
            if side_effect['counter'] == 0:
                self.remove_side_effect_tracker(change_type)

        if side_effect['type'] == 'append':
            side_effect['original_change']['side_effects'].append(side_effect_change)

    def remove_side_effect_tracker(self, change_type):
        side_effect = self.side_effects[change_type]
        to_pop = [change_type]
        if side_effect['link']:
            to_pop.append(side_effect['link'])
        for side_effect_type, data in self.side_effects.items():
            if data['link'] == change_type:
                to_pop.append(side_effect_type)

        for side_effect_type in to_pop:
            self.side_effects.pop(side_effect_type)

    def add_side_effect(self, change_type, original_change, counter, type):
        link = None
        if not counter:
            return
        elif isinstance(counter, str):
            if counter.split(':')[0] == 'link':
                link = counter.split(':')[-1]
                counter = self.side_effects[link]['counter']
        self.side_effects[change_type] = {'original_change': original_change, 'counter': counter, 'type': type, 'link': link}

    def on_change(self, change):
        if program_data['current_change']:
            self.clear_future()
        change_id = self.get_change_id()
        program_data['history'][change_id] = change
        program_data['history_order'].append(change_id)
        program_data['current_change'] = change_id
        program_data['signals'].history_changed.emit(change_id, change['type'], change['args'])

        if len(program_data['history_order']) > HISTORY_MAX_CHANGE_AMOUNT:
            change = program_data['history_order'][0]
            program_data['history'].pop(change)
            program_data['history_order'].remove(change)

    def clear_future(self):
        if len(program_data['history']) - 1 != program_data['history_order'].index(program_data['current_change']):
            index_of_current_change = program_data['history_order'].index(program_data['current_change'])
            to_remove = []
            for i in range(len(program_data['history_order']) - 1 - index_of_current_change):
                to_remove.append(program_data['history_order'][i + index_of_current_change + 1])
            for change in to_remove:
                program_data['history'].pop(change)
                program_data['history_order'].remove(change)

    def reverse(self, change):
        change_type = change['type']
        args = None
        run_function = True
        to_call = []
        if 'side_effect' in change_type:
            run_function = False

        if not change['reversed']:
            if not 'side_effect' in change_type:
                if   'add' in change_type:    change_type = change_type.replace('add', 'delete')
                elif 'delete' in change_type: change_type = change_type.replace('delete', 'add')
                elif 'change' in change_type: args = [change['reverse_args'][attribute] for attribute in self.args[change_type]]
            change['reversed'] = True
        else:
            change['reversed'] = False

        if 'side_effect' not in change_type:
            if not args: args = [change['args'][attribute] for attribute in self.args[change_type]]

        if 'side_effect' in change_type:
            if 'keyframes' in change_type and not 'position' in change_type:
                if   'deleted' in change_type: change_type = change_type.replace('deleted', 'added')
                elif 'added'   in change_type: change_type = change_type.replace('added',   'deleted')
                args = []
                for side_effect in change['side_effects']:
                    side_effect_type = side_effect['type']
                    if   'add' in    side_effect_type: side_effect_type = side_effect_type.replace('add', 'delete')
                    elif 'delete' in side_effect_type: side_effect_type = side_effect_type.replace('delete', 'add')
                    args.append([side_effect['args'][attribute] for attribute in self.args[side_effect_type]])
                to_call.append([self.functions[change_type], [args]])
                run_function = True
            else:
                for side_effect in change['side_effects']:
                    to_call.append([self.reverse, [side_effect]])

        elif 'add' in change_type:
            if 'effect' in change_type:
                self.add_side_effect('change_keyframe_value', change, change['args']['attribute_amount'], 'ignore')
            if run_function:
                to_call.append([self.functions[change_type], args])
            for side_effect in change['side_effects']:
                to_call.append([self.reverse, [side_effect]])

        elif 'delete' in change_type:
            if 'keyframe' in change_type and 'target' not in change_type:
                if (change['args']['frame'] not in program_data['keyframes'] or
                    change['args']['animator_id'] not in program_data['keyframes'][change['args']['frame']]):
                    run_function = False

            for side_effect in change['side_effects']:
                to_call.append([self.reverse, [side_effect]])
            if run_function:
                to_call.append([self.functions[change_type], args])

        elif 'change' in change_type:
            if 'effect_name' in change_type:
                self.add_side_effect(change_type, change, 1, 'ignore')
            if run_function:
                to_call.append([self.functions[change_type], args])
            for side_effect in change['side_effects']:
                to_call.append([self.reverse, [side_effect]])

        if run_function:
            self.add_side_effect(change_type, change, 1, 'ignore')


        for fun, args in to_call:
            fun(*args)

    def goto_change(self, direction):
        index = program_data['history_order'].index(program_data['current_change'])
        if direction == 'next':
            if index < len(program_data['history_order']) - 1: index += 1
        else:
            if index > 0: index -= 1
        program_data['current_change'] = program_data['history_order'][index]

    def undo(self):
        change = program_data['history'][program_data['current_change']]

        if not change['reversed']:
            self.reverse(change)
            self.goto_change('prev')

    def redo(self):
        if not program_data['history'][program_data['current_change']]['reversed']:
            self.goto_change('next')
            change = program_data['history'][program_data['current_change']]
        else:
            change = program_data['history'][program_data['current_change']]
            self.goto_change('next')

        if change['reversed']:
            self.reverse(change)

    def on_clear(self):
        program_data['history'] = {}
        program_data['history_order'] = []
        program_data['current_change'] = None
