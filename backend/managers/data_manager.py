from pathlib import Path
import inspect
import time

from definitions import program_data, QSS_DIR, DEFAULT_ANIMATION_SIZE, DEFAULT_FPS
from backend.signals import Signals

def manager_function_decorator(fun):
    def decorator(*args, **kwargs):
        inspect_frame = inspect.getouterframes(inspect.currentframe(), 2)[1]
        info = {'function_name': str(fun).split(' ')[1],
                'sender_name':   inspect_frame[1].split('/')[-1],
                'sender_method': inspect_frame[3],
                'call_time':     time.time(),
                'args':          args,
                'kwargs':        kwargs}
        program_data['stats']['function_calls'].append(info)
        program_data['manager'].logger_register_function_call(fun, args[1:], kwargs)

        start_time = time.time()
        result = fun(*args, **kwargs)
        info['process_time'] = time.time() - start_time

        return result
    return decorator

from backend.managers.attribute_value_manager_mixin import AttributeValueManagerMixin
from backend.managers.render_state_manager_mixin    import RenderStateManagerMixin
from backend.managers.generator_manager_mixin       import GeneratorManagerMixin
from backend.managers.keyframe_manager_mixin        import KeyframeManagerMixin
from backend.managers.playback_manager_mixin        import PlaybackManagerMixin
from backend.managers.logging_manager_mixin         import LoggingManagerMixin
from backend.managers.preview_manager_mixin         import PreviewManagerMixin
from backend.managers.history_manager_mixin         import HistoryManagerMixin
from backend.managers.render_manager_mixin          import RenderManagerMixin
from backend.managers.effect_manager_mixin          import EffectManagerMixin
from backend.managers.state_manager_mixin           import StateManagerMixin
from backend.managers.layer_manager_mixin           import LayerManagerMixin


class DataManager(         LayerManagerMixin,
                           StateManagerMixin,
                          EffectManagerMixin,
                          RenderManagerMixin,
                         LoggingManagerMixin,
                         PreviewManagerMixin,
                         HistoryManagerMixin,
                        KeyframeManagerMixin,
                        PlaybackManagerMixin,
                       GeneratorManagerMixin,
                     RenderStateManagerMixin,
                  AttributeValueManagerMixin):
    def __init__(self):
        program_data['manager'] = self
        program_data['signals'] = Signals()
        self.unique_id_counter = 0
        super(DataManager, self).__init__()

    def emit(self, signal, args=[]):
        '''
        Can we implement this with threading? we would need to use QThreads to make qt happy but they dont support args
        does qt have some way to automatically thread event handling?
        '''
        signal.emit(*args)

    def add_event_handler(self, event_handler_id, event_handler, parent):
        program_data['event_handlers'][event_handler_id] = event_handler(parent)

    def get_new_id(self):
        self.unique_id_counter += 1; return str(self.unique_id_counter)

    def get_font(self):
        return 'font-family: Roboto Mono; font-size: 12px; font-weight: Bold;'

    def get_stylesheet(self):
        with Path(QSS_DIR, 'main.qss').open() as f: result = f.read()
        return result

    def set_window_size(self, width, height):
        program_data['window_size'] = (width, height)
        program_data['signals'].window_size_changed.emit(width, height)

    def set_animation_width(self, width):
        self.set_animation_size(width, program_data['animation_size'][1])

    def set_animation_height(self, height):
        self.set_animation_size(program_data['animation_size'][0], height)

    def set_animation_size(self, width, height):
        program_data['animation_size'] = (width, height)
        program_data['signals'].animation_size_changed.emit(*program_data['animation_size'])

    def set_fps(self, fps):
        program_data['fps'] = fps
        program_data['signals'].fps_changed.emit(fps)

    def set_main_window(self, window):
        program_data['window'] = window
        program_data['signals'].window_changed.emit(window)

    def clear(self):
        program_data['signals'].clear.emit()
        program_data['manager'].set_fps(DEFAULT_FPS)
        program_data['manager'].set_animation_size(*DEFAULT_ANIMATION_SIZE)

    def shutdown(self):
        program_data['signals'].shutdown.emit()
        exit()
