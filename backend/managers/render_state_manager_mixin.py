from backend.event_handlers.render_state_event_handler import RenderStateEventHandler

from utils.copy_dict import copy_dict
from utils.delay import delay, delay_delay
from utils.dict_place_or_create import dict_place_or_create

from definitions import program_data, RENDER_STATE_UPDATE_DELAY


class RenderStateManagerMixin:
    def __init__(self, *args, **kwargs):
        super(RenderStateManagerMixin, self).__init__(*args, **kwargs)
        program_data['manager'].add_event_handler('render_state', RenderStateEventHandler, self)
        self.render_state_update_timer = None

    def get_render_state(self, frame, layer_id):
        render_state = []
        effect_attributes = {}
        for effect_id, effect in program_data['manager'].get_effects_in_layer(layer_id).items():
            if '1' not in program_data['keyframes'].keys(): break
            if effect_id not in program_data['keyframes']['1'].keys(): continue
            effect_attributes[effect_id] = {}
            for attribute_name in effect['attributes'].keys():
                effect_attributes[effect_id][attribute_name] = program_data['manager'].get_attribute_value_at_frame(frame, effect_id, attribute_name, size_matters=True)
        render_state.append(effect_attributes)
        render_state.append(program_data['animation_size'])
        render_state.append(copy_dict(program_data['layers'][layer_id]))

        if program_data['layers'][layer_id]['input_mode'] == 'Clone':
            related_layer_id = program_data['layers'][layer_id]['input_path']
            if frame in program_data['render_states'] and related_layer_id in program_data['render_states'][frame]:
                render_state.append(program_data['render_states'][frame][related_layer_id])
            else:
                render_state.append(self.get_render_state(frame, related_layer_id))

        return render_state

    def update_render_states(self):
        self.render_state_update_timer = None
        for frame in program_data['manager'].get_all_frames():
            for layer_id in program_data['layers']:
                if int(frame) > program_data['manager'].get_last_keyframe_for_layer(layer_id): continue
                frame = str(frame)
                render_state = self.get_render_state(frame, layer_id)
                if (frame in program_data['render_states'] and layer_id in program_data['render_states'][frame]
                    and render_state == program_data['render_states'][frame][layer_id]): continue
                else:
                    dict_place_or_create(program_data['render_states'], frame, layer_id, render_state)
                    program_data['manager'].emit(program_data['signals'].render_state_changed, [frame, layer_id, render_state])

    def send_update_signal(self):
        if not self.render_state_update_timer:
            self.render_state_update_timer = delay(self.update_render_states, RENDER_STATE_UPDATE_DELAY)
        else:
            delay_delay(self.render_state_update_timer, RENDER_STATE_UPDATE_DELAY)
