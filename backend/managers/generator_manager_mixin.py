from utils.odict_insert_at_index import odict_insert_at_index
from utils.min_max import min_max
import matplotlib.pyplot as plt
from math import pi, sin, cos
import numpy as np

from backend.managers.data_manager import manager_function_decorator

from utils.copy_dict import copy_dict

from definitions import program_data, GENERATOR_TEMPLATE, GENERATOR_SIGNALS

PI2 = pi * 2

sine_wave = lambda t, attributes: ((attributes['amplitude']
                                   * sin(PI2 * attributes['frequency'] * t + attributes['phase']))
                                   + attributes['offset'])

class GeneratorManagerMixin:
    def __init__(self, *args, **kwargs):
        super(GeneratorManagerMixin, self).__init__(*args, **kwargs)
        # program_data['manager'].add_generator(preset_id='2'); plt.plot(program_data['manager'].get_generator_wave(frame, generator_id)); plt.show(); exit()

    @manager_function_decorator
    def get_generator_wave(self, frame, generator_id, samples=16, cycles=1):
        attributes = program_data['manager'].get_generator_attributes(frame, generator_id)
        wave = []; t = 0; cycle = 1 / attributes['frequency']
        for s in range(samples*cycles):
            wave.append(min_max(sine_wave(t, attributes), -1, 1))
            t += cycle / (samples - 1)
        return wave

    @manager_function_decorator
    def get_generator_attributes(self, frame, generator_id):
        return {'amplitude': program_data['manager'].get_attribute_value_at_frame(frame, generator_id, 'Amplitude'),
                'frequency': program_data['manager'].get_attribute_value_at_frame(frame, generator_id, 'Frequency'),
                'phase'    : program_data['manager'].get_attribute_value_at_frame(frame, generator_id, 'Phase'    ),
                'offset'   : program_data['manager'].get_attribute_value_at_frame(frame, generator_id, 'Offset'   )}

    @manager_function_decorator
    def get_generator_value(self, frame, generator_id):
        attributes = program_data['manager'].get_generator_attributes(frame, generator_id)
        return min_max(sine_wave(int(frame) / program_data['fps'], attributes), -1, 1)

    @manager_function_decorator
    def add_generator(self, label=None, queue_order=None, preset_id=None, emit=True):
        if not preset_id:   preset_id   = program_data['manager'].get_new_id()
        if not queue_order: queue_order = len(program_data['generators'])
        if not label:       label       = f'signal {len(program_data["generators"])+1}'
        generator = copy_dict(GENERATOR_TEMPLATE)
        generator['type'] =       'Oscillator'
        generator['name'] =       label
        generator['attributes'] = GENERATOR_SIGNALS['Base']['attributes']

        generator_id = preset_id
        odict_insert_at_index(program_data['generators'], generator_id, generator, queue_order)

        to_add = []
        for attribute_name, attribute in generator['attributes'].items():
            to_add.append(['1', generator_id, attribute_name, attribute['value'], 'generator'])
        program_data['manager'].add_keyframes(to_add, emit=False)

        program_data['manager'].emit(program_data['signals'].generator_added, [generator_id, queue_order])
        program_data['manager'].emit(program_data['signals'].keyframes_adding, [len(generator['attributes'])])
        for attribute_name, attribute in generator['attributes'].items():
            program_data['manager'].emit(program_data['signals'].keyframe_added, ['1', generator_id, attribute_name, attribute['value'], 'generator'])

    @manager_function_decorator
    def delete_generator(self, generator_id):
        program_data['generators'].pop(generator_id)

        keyframes = copy_dict(program_data['manager'].get_keyframes(generator_id))
        to_delete = []
        for frame in keyframes:
            to_delete.append([frame, generator_id, None])
        count = program_data['manager'].delete_keyframes(to_delete, emit=False)

        program_data['manager'].emit(program_data['signals'].generator_deleted, [generator_id])
        program_data['manager'].emit(program_data['signals'].keyframes_deleting, [count])

        for frame in keyframes:
            for attribute_name in keyframes[frame][generator_id]:
                program_data['manager'].emit(program_data['signals'].keyframe_deleted,
                                             [frame, generator_id, attribute_name,
                                              keyframes[frame][generator_id][attribute_name], 'generator'])

    @manager_function_decorator
    def change_generator_type(self, generator_id, type, old_type):
        pass

    @manager_function_decorator
    def get_generators_by_connection(self, animator_id, attribute_name=None):
        result = []
        if attribute_name: check = lambda: attribute_name in generator['connections'][animator_id]
        else:              check = lambda: True
        for generator_id, generator in program_data['generators'].items():
            if animator_id in generator['connections']:
                if check(): result.append(generator_id)
        return result

    @manager_function_decorator
    def add_generator_connection(self, generator_id, animator_id, attribute_name):
        connection_id   = len(program_data['generators'][generator_id]['connections'])
        connection_name = f'C{connection_id}Amplitude'

        if animator_id not in program_data['generators'][generator_id]['connections']:
            program_data['generators'][generator_id]['connections'][animator_id] = {}

        if connection_name not in program_data['generators'][generator_id]['attributes']:
            program_data['generators'][generator_id]['attributes'][connection_name] = {
                'type': 'float', 'value': 1, 'min': 0,  'max': 1}

        if attribute_name not in program_data['generators'][generator_id]['connections'][animator_id]:
            program_data['generators'][generator_id]['connections'][animator_id][attribute_name] = {'Amplitude': connection_name}
            program_data['manager'].add_keyframe('1', generator_id, connection_name,
                                                 program_data['generators'][generator_id]['attributes'][connection_name]['value'],
                                                 'generator')
            program_data['manager'].emit(program_data['signals'].generator_connection_added, [generator_id, animator_id, attribute_name])
