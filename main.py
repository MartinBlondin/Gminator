'''
Gator: a G'MIC based animator

author - MartinBlondin@ProtonMail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import sys
import traceback
import logging

import PyQt5.QtWidgets as QW

from definitions import program_data, DEBUG
from utils.render_qml import render_qml
if DEBUG: render_qml()

from widgets.main_window import MainWindow

def run_application(app):
    app.exec_()
    program_data['manager'].shutdown()

def start_window():
    logging.basicConfig()
    app = QW.QApplication(sys.argv)
    a = MainWindow(app.desktop())
    sys.exit(run_application(app))

if __name__ == '__main__':
    if DEBUG:
        start_window()
    else:
        try:
            start_window()
        except Exception as error:
            program_data['logger'].setLevel(logging.INFO)
            program_data['logger'].exception(error)
            # Todo implement bug reporting dialog
            info = '| I currently have no bug reporting features, if you could raise an issue on Gitlab |'
            program_data['logger'].info('\n')
            program_data['logger'].info('-' * len(info))
            program_data['logger'].info('|                                      README                                       |')
            program_data['logger'].info('-' * len(info))
            program_data['logger'].info(info)
            program_data['logger'].info('| with this gibbelygook copy pasted it would be much appreciated                    |')
            program_data['logger'].info('|                                                                                   |')
            program_data['logger'].info('| https://gitlab.com/MartinBlondin/Gator                                            |')
            program_data['logger'].info('-' * len(info))
