from utils.copy_dict import copy_dict
from collections import OrderedDict

def odict_insert_at_index(odict, item_key, item_value, index, overwrite=True):
    if index < len(odict):
        result = OrderedDict()
        left   = list(odict)[:index]
        right  = list(odict)[index:]

        if overwrite:
            to_add = []
            for key in right: to_add.append([key, odict[key]]); odict.pop(key)
            odict[item_key] = item_value
            for key, item in to_add: odict[key] = item
        else:
            for key in left: result[key] = odict[key]
            result[item_key] = item_value
            for key in right: result[key] = odict[key]

    else:
        result = odict
        result[item_key] = item_value
        if overwrite:
            odict[item_key] = item_value

    return result
