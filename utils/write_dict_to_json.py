import demjson

def write_dict_to_json(data, fileName, append=False):
    if append:
        with open(fileName, 'a+') as f:
            f.write(demjson.encode([data]))
    else:
        with open(fileName, 'w+') as f:
            f.write(demjson.encode([data]))
