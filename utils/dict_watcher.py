import sys
from pprint import PrettyPrinter as PP

from pathlib import Path

from PyQt5.QtCore import Qt
import PyQt5.QtWidgets as QW

from utils.copy_dict import copy_dict
from utils.delay import delay


class DictWatcher(QW.QWidget):
    def __init__(self, dictionary, focused_keys=None, style=None, *args, **kwargs):
        super(DictWatcher, self).__init__(*args, **kwargs)
        self.dictionary = dictionary
        self.setLayout(QW.QVBoxLayout())
        self.layout().setContentsMargins(2, 2, 2, 2)

        if style: self.setStyleSheet(style)

        self.width = 200
        self.height = 400
        self.focused_keys = focused_keys
        self.text = None
        self.update_geometry()
        self.update_text()
        self.show()

        self.last_rendered_dict = None
        self.dict_check_timer = delay(self.check_dict, 500)
        self.move(0, 0)

    def update_text(self):
        if self.focused_keys:
            if isinstance(self.focused_keys, list):
                dict_str = ''
                for key in self.focused_keys:
                    dict_str += '{}:\n{}\n\n'.format(key,
                                                     PP(indent=1).pformat(self.dictionary[key]))
            else:
                dict_str = PP(indent=1).pformat(self.dictionary[self.focused_keys])
        else:
            dict_str = PP(indent=1).pformat(self.dictionary)
        if self.text:
            scroll_value = self.text.verticalScrollBar().value()
            self.text.document().setPlainText(dict_str)
            self.text.verticalScrollBar().setValue(scroll_value)
        else:
            self.text = QW.QPlainTextEdit(dict_str)
            self.text.setTextInteractionFlags(Qt.NoTextInteraction)
            self.text.setFocusPolicy(Qt.NoFocus)
            self.layout().addWidget(self.text)

        widest_line = 0
        for line in dict_str.split('\n'):
            if len(line) > widest_line:
                widest_line = len(line)
        self.width = widest_line * 8
        self.height = self.width / 1.618

        self.update_geometry()

    def update_geometry(self):
        if self.width > 600: self.width = 600
        if self.height > 400: self.height = 400
        self.setFixedWidth(self.width)
        self.setFixedHeight(self.height)
        self.move(0, 0)

    def check_dict(self):
        if self.dictionary != self.last_rendered_dict:
            self.last_rendered_dict = copy_dict(self.dictionary)
            self.update_text()
        self.dict_check_timer = delay(self.check_dict, 1000)

    def close(self):
        self.deleteLater()
