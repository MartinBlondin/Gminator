'''
used by QGraphicsScene components reorder which items gets rendered first
'''

def display_on_top(item, scene):
    scene.removeItem(item)
    scene.addItem(item)
