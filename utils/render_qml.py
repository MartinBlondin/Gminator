import os
from pathlib import Path
from sh import pyuic5

from definitions import QML_DIR, RENDERED_QML_DIR

def render_qml():
    for qml_file in Path(QML_DIR).iterdir():
        if '.ui' in qml_file.parts[-1]:
            python_file = Path(RENDERED_QML_DIR, qml_file.parts[-1].replace('.ui', '.py'))
            try:
                pyuic5('-x', qml_file.resolve(), '-o', python_file.resolve())
            except:
                pass
