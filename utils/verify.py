import traceback
import pprint

from definitions import DEBUG, program_data
from utils.decorator_with_args import decorator_with_args

LOG = print

@decorator_with_args
def verify(fun, verify_lambda):
    def aux(*xs, **kws):
        verified = True
        if DEBUG:
            errors = []

            for description, rule in verify_lambda(*xs).items():
                if rule:
                    verified = False
                    trace = traceback.format_stack()
                    errors.append(description)

        if verified:
            return fun(*xs, **kws)
        else:
            LOG('{}{}\n'.format('\n'.join(trace), '-' * (18 + len(str(fun)))))
            pprint.PrettyPrinter(indent=2).pprint(program_data)
            LOG('{}\nVerificationError: {}\nErrors: {}\n{}\n'.format('-' * (18 + len(str(fun))),
                                                                       fun,
                                                                       ', '.join(errors),
                                                                       '-' * (18 + len(str(fun)))))
            exit()
    return aux
