from threading import Thread
from time import sleep

from utils.decorator_with_args import decorator_with_args

class RateLimiter:
    '''
    Allows you to specify time and identifier to limit the amount of calls to a function

    Sends the last call after specified time has passed between calls
       if calls are constantly made before the timeframe has ended it will never send any calls

    Not to be used on functions with returns
    '''
    running_functions = {}
    stop_signals = {}
    unique_id = 0

    def run_function(fun, time, identifier, unique_id, xs, kws):
        sleep(time)
        if not RateLimiter.stop_signals[unique_id]:
            fun(*xs, **kws)
            RateLimiter.running_functions.pop(identifier)
        RateLimiter.stop_signals.pop(unique_id)

    @decorator_with_args
    def rate_limit(fun, self, time, identifier, *args, **kwargs):
        def aux(*xs, **kws):
            ident = str(fun)
            if identifier:
                ident += str(identifier(*xs, **kws))
            unique_id = RateLimiter.unique_id
            RateLimiter.unique_id += 1

            if ident in RateLimiter.running_functions.keys():
                RateLimiter.stop_signals[RateLimiter.running_functions[ident]['last_unique_id']] = True

            RateLimiter.running_functions[ident] = {}
            RateLimiter.running_functions[ident]['last_unique_id'] = unique_id
            RateLimiter.stop_signals[unique_id] = False

            Thread(target=RateLimiter.run_function, args=(fun, time, ident, unique_id, xs, kws)).start()

            return None
        return aux

rate_limit = RateLimiter().rate_limit
